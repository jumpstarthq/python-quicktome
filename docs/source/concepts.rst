.. _concepts:

********
Concepts
********

QuickTome uses a number of conventions for managing, loading, and parsing content into its final form. The Pandoc
utility is used for the conversion.

Workflow
========

The general workflow for working with a QuickTome project may be described as ...

1. **Create** content files (in Markdown), images (jpg, png), and data (csv).
2. **Compile** and collect these files into a single input file suitable for use by Pandoc.
3. **Convert** the input file to one or more of the supported output formats.

.. image:: _static/images/create-compile-convert.png

Of these, QuickTome completely automates steps 2 and 3, and provides assistance for managing step 1.

.. note::
    Why *collect and compile*? It is far easier to maintain all but the smallest projects as multiple files, and while
    it is possible to pass multiple input files to the ``pandoc`` command, it is far more efficient to first combine
    these files into just one file that may be passed to Pandoc. This also allows inspection of the combined project,
    and facilitates a number of QuickTome's more advanced features.

Sub-Commands
============

QuickTome is a command-line tool which defines various sub-commands that may be used to interact with a project. Use the
``quicktome --help`` to see a list of commands. See :ref:`commands`.

.. figure:: _static/images/sandbox-screen-shot.jpg

    A Screen Shot From Our QuickTome Sandbox

Input-Output Formats
====================

The *input* format refers to the file used by Pandoc to export content to the various *output* formats. When the
``build`` sub-command is executed, QuickTome first "compiles" your content files into a single input file. Then Pandoc
is used to convert to the desired :ref:`output`.

Markdown is currently the only supported input format in QuickTome.

.. note::
    Early versions of QuickTome also supported reStructuredText, but during testing we found that Pandoc's support for
    reStructuredText is limited. If you prefer that format, you may find that tools such as `Sphinx`_ will meet your
    project's needs.

.. _Sphinx: http://www.sphinx-doc.org/en/master/

Loaders and Structuring a Project
=================================

QuickTome strives to support virtually any project structure by providing a "loader" system for finding content. The
loader is responsible for finding images, text, and templates to make them easy to prepare for the final output.

There are currently three types of loading strategies:

1. Manifest: Where the files to be loaded are listed in a plain text file, the ``MANIFEST.txt``.
2. OPML: Where a pre-defined OMPL file may be used to define the files to be loaded. OPML is an XML format with a number
   of editors available. For example, `OmniOutliner`_ can export OPML.
3. Tree: Where the structure of the project controls the order of the files to be loaded. This is accomplished by
   naming files and/or directories with a number such as ``01``, ``02``, ``03`` and so on. The leading ``0`` is required
   for proper sorting of single-digit numbers.

.. _OmniOutliner: https://www.omnigroup.com/omnioutliner

Additionally, any content file may use the ``include`` and ``table`` directives (see :ref:`content-directives`) to import
content from other files. Included files may in turn make use of ``include`` and ``table``, making it possible (for
example) to refer to only one file in the ``MANIFEST.txt`` or ``outline.ompl``, causing all other files to be included.

.. tip::
    If you aren't seeing the expect output, try using the ``-D`` switch to enable debug mode, which will print all of
    the files that are collected and loaded.

.. _context:

The Context
===========

Each time QuickTome builds a project, it first loads a *context* which is a collection of variables that may be used
to control the output. The context may be used to print variables and conditionally display areas of content.

See :ref:`content-directives`, :ref:`content-parsing`, and :ref:`settings`.

.. _concerning-templates:

Templates and Archetypes
========================

In the QuickTome documentation, a *template* always refers to a file that controls or adds to specific output formats.
See :ref:`output`.

*Archetypes*, on the other hand, are templates for *content* files. See :ref:`content-archetypes`.

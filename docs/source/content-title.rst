.. _content-title:

**********
Title Page
**********

There are a couple of ways to create a title page:

1. `Using YAML`_
2. `Using Markdown`_

Using YAML
==========

Create a ``title`` file (with the normal Markdown extension) and make sure this file is included first.

.. code-block:: yaml

    ---
    title: Project Title
    author: Author Name
    ---

Pandoc will automatically generate a title page at the beginning of the output.

Using Markdown
==============

You may also use Markdown in combination with LaTeX or :ref:`content-directives`.

.. code-block:: text

    \newpage


























    \begin{center}

    {{ project.title }}

    Edition {{ project.edition }}

    Version {{ current_version }}

    Published {{ current_month }}, {{ current_year }}

    \end{center}


This page typically is loaded first, but may exist in any order available to your selected loader
(see :ref:`content-structure`). For example, after a dedication page.

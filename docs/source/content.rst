******************
Content Management
******************

.. toctree::
    :maxdepth: 2

    Structure <content-structure>
    Archetypes <content-archetypes>
    Markdown <content-markdown>
    Images <content-images>
    Includes <content-includes>
    Data <content-data>
    Slideshows <content-slideshows>
    Directives <content-directives>
    Parsing <content-parsing>
    Bibliography <content-bibliography>
    Hooks <content-hooks>
    Title Page <content-title>
    Cover Page <content-cover>

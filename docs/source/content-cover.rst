.. _content-cover:

***********
Cover Pages
***********

The ``epub`` (and by extension ``mobi``), ``pdf``, and ``printable`` output formats support cover pages.

Location of Covers
==================

Cover files must be saved to the appropriate ``templates/<output_format>//`` directory:

- ``epub``: ``templates/epub/cover.jpg``
- ``pdf``: ``templates/pdf/cover.pdf`` (the first page is used for the cover)
- ``printable``: ``templates/printable/cover.pdf`` (the first page is used for the cover)

Creating Covers
===============

Sizing Covers
-------------

PDF files used for covers should match the size and orientation of the output. For example, if the output is 8.5 x 11
inches (the default), the ``cover.pdf`` must be the same.

The EPub ``cover.jpg`` dimensions should also match the size. For 8.5 x 11 inches, this would be 2550 x 3330 pixels.

Cover Resolution
----------------

Images and image content (within PDFs) should be 300 pixels per inches for screen quality output, and a minimum of 150
pixels per inch for print-quality.

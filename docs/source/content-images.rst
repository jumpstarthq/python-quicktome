.. _content-images:

******
Images
******

Location of Images
==================

Images may exist anywhere in the structure of your content provided they are always contained within an ``_images/``
directory.

For more information see :ref:`content-structure` and especially :ref:`content-structure-images`.

Defining Images in Content
==========================

For more information on images in Markdown files, see `Pandoc's handling of image Markdown`_.

.. _Pandoc's handling of image Markdown: https://pandoc.org/MANUAL.html#images

Image rendered without a caption and alt text:

.. code-block:: text

    ![](_images/example.jpg)

Image rendered with a caption:

.. code-block:: text

    ![This is the Caption](_images/example.jpg)

Image rendered with both a caption and alt text:

.. code-block:: text

    ![This is the Caption](_images/example.jpg "This is the Alt Text")

Image rendered inline:

.. code-block:: text

    ![](_images/example.jpg)\

Image rendered from a reference:

.. code-block:: text

    ![example image]

    [example image]: _images/example.jpg

Wrapping Text Around Images
===========================

QuickTome does not directly support image wrap. It is, however, possible to specify the Latex for this instead.

First, you must add ``\usagepackage{wrapfig}`` at the top of your ``default.tex`` file.

Next, you may add a wrapped image like so:

.. code-block:: text

    \begin{wrapfigure}{R}{0.3\textwidth}
    \centering
    \includegraphics[width=0.5\textwidth]{_images/example.jpg}
    \caption{\label{fig:example}Example Image}
    \end{wrapfigure}

The first line uses ``{L}`` for left alignment and ``{R}`` for right alignment. The numeric values may require some amount of experimentation based on the image and surrounding text.
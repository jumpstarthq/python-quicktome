***************
Getting Started
***************

System Requirements
===================

Python
------

Python 3.6 or later is required.

Pandoc
------

QuickTome utilizes the ``pandoc`` command to convert files to various formats. Version 2.5 or later is required. See
`Pandoc's installation instructions`_.

.. _Pandoc's installation instructions: https://pandoc.org/installing.html

.. tip::
    If you are on a Mac and using `Homebrew`_, you may install pandoc using ``brew install pandoc``.

.. _Homebrew: https://brew.sh

Output-Specific Requirements
----------------------------

KindleGen
.........

KindleGen is used to create MOBI output. It is a proprietary but free tool provided by Amazon and is available
for all platforms. See `the KingleGen manual`_.

.. _the KingleGen manual: https://kindlegen.s3.amazonaws.com/manual.html

LaTeX
.....

LaTeX is required for PDF and printable PDF output. `LaTeX is available for Linux, Mac, and Windows`_.

The LaTeX package for Mac is MacTeX and is quite large (~4GB). Even so, we install it to be sure and have access to the
full array of LaTeX capabilities. However, the Pandoc installation guide recommends installing just BasicTeX or TinyTeX,
and using the ``tmlgr`` utility to install packages as needed.

.. _LaTeX is available for Linux, Mac, and Windows: https://www.latex-project.org/get/

Installation
============

To install QuickTome, run ``pip install python-quicktome`` from the command line.

.. tip::
    You may wish to customize your :ref:`settings`.

Steps
=====

1. Create Project Directory
---------------------------

Start by creating the project directory.

.. code-block:: bash

    cd $QUCKTOME_PROJECT_HOME;
    mkdir quicktome-test;

2. Run the Initialization Command
---------------------------------

Next create the project structure and default files.

.. code-block:: bash

    cd quicktome-test;
    quicktome init;

This results in the following structure:

.. code-block:: text

    quicktome-test
    |-- assets
    |-- meta.ini
    |-- output
    |-- source
    |   |-- MANIFEST.txt
    |   |-- archetypes
    |   |   |-- chapter.ini
    |   |   |-- chapter.markdown
    |   |   |-- contact.markdown
    |   |   |-- part_N.markdown
    |   |   `-- title.markdown
    |   |-- content
    |   |   |-- _data
    |   |   `-- _images
    |   |-- hooks
    |   |-- settings
    |   `-- templates
    |       |-- docx
    |       |   `-- default.docx
    |       |-- epub
    |       |   `-- epub.css
    |       |-- html
    |       |   |-- footer.html
    |       |   `-- header.html
    |       |-- pdf
    |       |   `-- default.tex
    |       `-- printable
    |           `-- default.tex
    `-- temporary

.. tip::
    Use ``quicktome init -r quicktome-test`` from the ``$QUICKTOME_PROJECT_HOME`` (or any other location) to skip
    step 1.

3. Create Some Content
----------------------

Open your favorite editor to the project directory and create new files. You may also create new files using ``new``
command.

.. code-block:: bash

    quicktome new path/to/new-file;

This uses the default archetype (``chapter``) to start a new file. The extension is automatically added using your
specified input format. To specify an archetype, use the ``-a`` switch:

.. code-block:: bash

    quicktome new -a part_N path/to/part-1;

4. Next Steps
-------------

A) Create Data and Image Files
..............................

Create your data (CSV or INI) files in the ``source/content/_data/`` directory. See :ref:`content-data`.

.. tip::
    You may also locate data or image files anywhere in the project structure under ``content/``.

Create and store your image files within your content structure. See :ref:`content-images`.

B) Customize Output Templates
.............................

The various output formats are supported by the files found under ``source/templates/``. Customize these as needed.

See :ref:`concerning-templates`.

C) Create or Customize Archetypes
.................................

Archetype files are used by the ``quicktome new`` command. Customize the provided starter files to your liking, or
create new files that are specific to your project.

See :ref:`content-archetypes`.

Reference
=========

- `KingleGen on the MobileRead Wiki`_
- `LaTeX Home Page`_
- `Markdown (Daring Fireball)`_
- `Markdown Cheat Sheet (markdownguide.com)`_
- `Pandoc Home Page`_

.. _KingleGen on the MobileRead Wiki: https://wiki.mobileread.com/wiki/KindleGen
.. _LaTeX Home Page: https://www.latex-project.org
.. _Markdown (Daring Fireball): https://bookdown.org
.. _Markdown Cheat Sheet (markdownguide.com): https://www.markdownguide.org/cheat-sheet
.. _Pandoc Home Page: https://pandoc.org/

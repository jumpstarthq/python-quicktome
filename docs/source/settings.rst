.. _settings:

********
Settings
********

Settings may be used to control QuickTome's default behavior for a number of the available sub-commands. Such
configuration options fall into one of three categories:

1. `Environment Variables`_
2. `Meta File`_ Settings
3. `Ad-Hoc Settings`_

When adding or changing settings, you should understand the `Load Order`_.

Environment Variables
=====================

`A number of environment variables`_ may be configured that automate or control QuickTome behavior.

.. _A number of environment variables: https://en.wikipedia.org/wiki/Environment_variable

``QUICKTOME_AUTHOR_NAME``
-------------------------

Used when creating a new project with the ``init`` command.

Default: ``None``

``QUICKTOME_AUTHOR_TITLE``
--------------------------

Used when creating a new project with the ``init`` command.

Default: ``None``

``QUICKTOME_COPYRIGHT_NAME``
----------------------------

Used when creating a new project with the ``init`` command.

Default: ``QUICKTOME_AUTHOR_NAME``

``QUICKTOME_INPUT_FORMAT``
--------------------------

The default input format to use when running the ``init`` command.

Choices: ``Markdown``, ``reStructuredText``

Default: ``Markdown``

``QUICKTOME_LOGGER_NAME``
-------------------------

Primarily for programmatic use of the QuickTome library, this allows developers to change the logger used by QuickTome
when log statements are issued.

Default: ``quicktome``

``QUICKTOME_PROJECT_HOME``
--------------------------

Used by the ``ls`` command to locate QuickTome projects.

Default: ``None``

.. tip::
    If you are a Virtual Env User, the default will be that of ``$PROJECT_HOME``.

Meta File
=========

The ``meta.ini`` file exists at the root of the project and may be used to control various aspects of the project.

.. tip::
    ``quicktome init`` automatically creates this file for you.

Pre-Defined Settings
--------------------

QuickTome defines a number of configuration sections. These are created automatically by the ``init`` sub-command.

Settings Used in Context
........................

All configuration sections found in the ``meta.ini`` file may be used as context when your content files are combined.
These may be used in your content to refer to variables, for example ``{{ author.name }}`` would resolve to the ``name``
variable of the ``author`` section. See :ref:`content-directives-variables`.

The standard sections and attributes are:

.. code-block:: ini

    [author]
    name = Author Name
    title = Author Title

    [copyright]
    name = Copyright Holder
    year = YYYY

    [project]
    description = Description of the project.
    edition = 1
    title = Project Title


Settings Used for Output
........................

A number of settings my also be defined which control options for the various output formats. These are (with defaults):

.. code-block:: ini

    [docx]
    template = templates/reference.docx

    [epub]
    cover = templates/epub.jpg
    stylesheet = templates/epub.css
    toc = True

    [html]
    footer = templates/footer.html
    header = templates/header.html
    toc = True

    [mobi]
    ; no additional options

    [pdf]
    cover = True
    font_family = bookman
    font_size = 24
    template = templates/default.tex
    toc = True

    [printable]
    cover = True
    font_family = bookman
    font_size = 14
    template = templates/printable.tex
    toc = True

    ; not fully implemented
    [slideshow]
    format = reveal
    level = 2
    theme = league

Ad-Hoc Settings
===============

Using the -V Switch
-------------------

Variables may be passed to QuickTome using the `-V` switch.

.. code-block:: bash

    quicktome build pdf -V audience:Programmers

This makes the ``audience`` variable available in the context.

An Extra Variables File
-----------------------

It is possible to specify an additional settings file with the ``-E`` or ``--extras`` switches. This is useful when you
have a collection of variables that may be used for the objectives a specific build. For example, if you build a project
for different audiences.

.. tip::
    Store this file in the ``source/settings/`` directory.

The format is identical to that of the ``meta.ini`` file.

.. code-block:: ini

    ; settings/advanced-users.ini
    [audience]
    experienced = yes
    ruleset = advanced

Load Order
==========

It can be important to understand the order in which settings are loaded, especially when those variables are used
for :ref:`content-parsing`. Variables are loaded as follows.

1. "Current" variables.
2. Settings from ``meta.ini``.
3. Variables loaded from `An Extra Variables File`_.
4. Variables set with the ``-V`` switch.

Variables added via the extra variables file or the ``-V`` switch may override any previously set variables.

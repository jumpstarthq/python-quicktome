************
Dependencies
************

System Packages
===============

The `pandoc utility`_ is the underlying source of file conversion. See the `Pandoc installation page`_.

.. _Pandoc installation page: https://pandoc.org/installing.html
.. _pandoc utility: https://pandoc.org

Python Packages
===============

Awesome Slugify
---------------

The `awesome-slugify package`_ is used to generate unique file names for output files.

.. _awesome-slugify package: https://github.com/voronind/awesome-slugify

Jinja2
------

Used pre-processing content files.

Markdown
--------

Obviously.

My Ninjas
---------

Provides a plethora of useful utilities.

OPML
----

The `python-opml package`_ is used to parse OPML files for loading content.

.. _python-opml package: https://pypi.org/project/opml/

PyTest
------

Used for unit tests.

Sphinx
------

Sphinx is used for documentation along with ``sphinx_rtd_theme`` and `sphinx-helpers`_.

.. _sphinx-helpers: https://github.com/develmaycare/sphinx-helpers

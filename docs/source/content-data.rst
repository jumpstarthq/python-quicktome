.. _content-data:

**********
Data Files
**********

Data files are CSV files that may be imported and converted to the Markdown format when the work is collected and
compiled.

Location of Data Files
======================

Data files may be located anywhere in the directory structure of your content.

- A file relative to the file in which the table is referenced.
- A sub-directory relative to the final, such as ``_data/``.
- Finally, data may also be stored in the ``source/content/_data/`` directory.

Defining Data in Content
========================

QuickTome provides a mechanism for the data to be loaded, similar to reStructuredText.

.. note::
    Although the directive is similar to reStructuredText, no additional options are supported.

To refer to a table:

.. code-block:: text

    .. table:: example.csv

The extension may be omitted:

.. code-block:: text

    .. table:: example

Data may also be placed in a directory relative to the file in which the statement is made:

.. code-block:: text

    .. table:: _data/example.csv

Again, the extension may be omitted.

Inline Tables
-------------

It is also possible to specify a table within the document by providing the content immediately after the table
directive.

.. code-block:: text

    .. table::
    Breed,Origin,Classification
    Chihuahua,Mexico,Companion Dog
    Kelpie,Australia,Herding Dog
    Siberian Husky,Russia,Northern Breed

The inline table ends with the first blank line.

.. note::
    Quoted CSV lines are *not* supported.

Table Captions
--------------

You may add a caption to the table by adding ``Table: Caption`` before or after the table directive:

.. code-block:: text

    Table: Example Data (Appears Before the Table)

    .. table:: example

Or ...

.. code-block:: text

    .. table:: example

    Table: Example Data (Appears After the Table)

QuickTome
=========

QuickTome is a content creation and management tool for printable books, e-books, and PDFs. It takes multiple
images and Markdown files and compiles them into a single file before conversion to a variety of formats.

The benefits include:

- Set up a new project in under a minute with a single command. See :ref:`commands` and :ref:`content-skeletons`.
- Quickly create new content from pre-defined content templates. See :ref:`content-archetypes`.
- Include other files (:ref:`content-includes`) and load Markdown tables from CSV (:ref:`content-data`)
- Utilize special directives for annotations, background images, page breaks, text replacement, and more. See
  :ref:`content-directives`.
- Use variables and conditions within your content for dynamic control of the build. See :ref:`content-parsing`.


Contents:

.. toctree::
    :maxdepth: 2

    Getting Started <getting-started>
    Concepts <concepts>
    Content Management <content>
    Output <output>
    Commands <commands>
    Settings <settings>
    Resources <resources>
    Developer <developer>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

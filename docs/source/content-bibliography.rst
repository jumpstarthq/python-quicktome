.. _content-bibliography:

************
Bibliography
************

QuickTome will automatically pass the ``--bibliography`` switch to Pandoc if a file named ``bibliography`` exists in the
``source/`` directory. It must include one of the extensions below.

.. note::
    This requires an additional install of the ``pandoc-citeproc`` filter, e.g. ``brew install pandoc-citeproc`` on a
    Mac (using Homebrew). See the `Pandoc installation guide`_.

.. _Pandoc installation guide: https://pandoc.org/installing.html

The supported extensions are:

.. csv-table:: Bibliography Extensions
    :header: Format, Extension

    BibLaTeX,.bib
    BibTeX,.bibtex
    Copac,.copac
    CSL JSON,.json
    CSL YAML,.yaml
    EndNote,.enl
    EndNote XML,.xml
    ISI,.wos
    MEDLINE,.medline
    MODS,.mods
    RIS,.ris

See `Pandoc citations`_.

.. _Pandoc citations: https://pandoc.org/MANUAL.html#citations

.. _content-archetypes:

**********
Archetypes
**********

An *archetype* is a template for content. Using the ``new`` sub-command, a new content file may be quickly created.
For example:

.. code-block:: bash

    quicktome new chapter-5;

After prompting you for a few options, this will create ``content/chapter-5.markdown`` using the default ``chapter``
archetype.

To use a specific archetype, use the ``-A`` option:

.. code-block:: bash

    quicktome new part_1 -A part;

.. tip::
    Use ``quicktome inv -A`` to list the available archetypes.

Location of Archetypes
======================

Archetypes are located in the ``source/archetypes/`` directory using the same file extension as that assigned to your
project.

Using Existing Archetypes
=========================

QuickTome provides a handful of archetypes to get your started. These are:

- ``chapter``: A new chapter file.
- ``contact``: An author contact page.
- ``part_N``: A page that may be used to delineate multi-part projects.
- ``title``: A title page.

Of these, only ``chapter`` will prompt for additional information before creating the content. This is because the
other archetypes, when used as content, will be parsed before the final output is generated.

Creating New Archetypes
=======================

It's easy to create a new archetype. Follow these steps:

1. Copy an existing file or create a new file in the ``archetypes/`` directory. The file name may not contain spaces or
   special characters except underscores or dashes. We also recommend the file name be lower case.
2. Add placeholder or boilerplate content as needed.
3. If you wish to be prompted for additional information when creating new content, you should also create another file
   of the same name, but with the ``.ini`` extension. See below.

Enabling Prompts for New Content
--------------------------------

To enable prompts for new content, you need an INI file with the same file name as the archetype. For example, the
default ``chapter.markdown`` archetype has a corresponding ``chapter.ini`` file.

As an example, the content of ``chapter.markdown`` looks like this:

.. code-block:: text

    .. break::

    # Chapter {{ number }}. {{ title }}

    {% if include_lorem %}

    > Quote

    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nulla sed mauris id
    felis fermentum cursus. Proin vulputate. Maecenas lobortis. Nullam ornare
    lacinia diam. Quisque lobortis metus ac ante.

    ## Section 1

    ### Sub Section A

    ...
    {% endif %}

And now the content of ``chapter.ini``:

.. code-block:: ini

    [include_lorem]
    default = yes
    label = Include Lorem
    type = boolean

    [number]
    label = Chapter Number
    required = no
    type = integer

    [title]
    label = Chapter Title
    required = yes
    type = string

The prompts above are used to create the new file.

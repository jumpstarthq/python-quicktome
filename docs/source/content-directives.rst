.. _content-directives:

**********
Directives
**********

Directives are special statements within content that may be pre-processed or used when generating output.

How Directives Are Processed
============================

All directives are discovered and parsed prior to passing the content to Pandoc for conversion. This allows both HTML
and LaTeX to be inserted into the output stream, but it does impose conditions.

Caveats
-------

*With PDF and Printable output*, LaTeX is used to format the text. This results in a couple of known limitations.

1. The first is that special characters (such as the ampersand) may cause the compiler to fail, producing an error like
    ``Misplaced alignment tab character &``.
2. The second is that Markdown contained with the line will *not* be processed.

*With EPub and HTML output*, HTML is used to format the text, typically with one or more CSS classes attached. The
default templates ``epub.css`` and ``header.html`` (installed with the ``init`` sub-command) provide styling, but you
may wish to customize this to your liking.

Directives that Wrap Content
----------------------------

Directives that currently wrap their content include:

- `Annotations`_
- `Standout Text`_

Using Directives
================

Anchors
-------

Anchors allow shortcuts to be used in place of a full description.

.. important::
    Anchors must always be unique across the entire project.

To define an anchor:

.. code-block:: text

    .. a:: example-anchor

To use an anchor in your content:

.. code-block:: text

    Please refer to [the example anchor][#example-anchor] for more information.

.. note::
    Pandoc supports this approach to anchors for HTML and EPub output formats. See `Replacement Text`_.

Annotations
-----------

An annotation is a callout within the text that draws attention to something notable. A number of annotations are
supported by default and are all invoked in the same manner.

.. code-block:: rst

    .. important:: Important!
        This text draws attention to something important.

The title of the callout is optional.

The supported annotations are:

- ``callout``
- ``important``
- ``info``
- ``note``
- ``tip``
- ``warning``

.. tip::
    Internally, QuickTome detects the directive and converts it to LaTeX. Special environments have been defined in the
    ``default.tex`` files using ``\newtcolorbox``. See `tcolorbox`_. Thanks goes to `Gonzalo Medina`_ for pointing us in
    the right direction.

.. _Gonzalo Medina: https://tex.stackexchange.com/a/59342/177860
.. _tcolorbox: https://ctan.org/pkg/tcolorbox

Background Images
-----------------

You may include a background image on any given page using the ``background`` directive:

.. code-block:: rst

    .. background:: example.png

You may also specify additional options in comma-separated format:

.. code-block:: rst

    .. background:: example.png, angle=0, height=10, placement=top, scale=2, width=10

These options are:

- ``angle``: An integer representing the rotation of the image. Default: ``0``.
- ``height``: Specify as ``full`` or in centimeters. Default: ``None``
- ``opacity``:
- ``placement``: ``top``, ``middle``, ``bottom``.
- ``scale``: Default: ``1``
- ``width``: Specify as ``full`` or in centimeters. Default: ``None``

.. tip::
    An 8.5in x 11in page is 21.59cm x 27.94cm.

Blank Page
----------

Insert a blank page using ``.. blank::``.

Comments
--------

Comment directives are for leaving internal comments or editor or "critic" notes and so on. Comments do *not* appear in
output, but may be exported using the ``comments`` sub-command.

.. code-block:: text

    .. comment:: This is a comment.

    The quick brown fox jumped over the lazy dog.

Comments must appear as a single line.

Drop Caps
---------

Insert a drop cap with the ``drop`` directive.

.. code-block:: text

    .. drop::This is the first line of a paragraph. It will be displayed as a drop cap which adds a bit of
    flare to the text, don't you think? Note that this will only work if the font in use is scalable.

Note that, unlike most other directives, the ``drop`` command must be included directly next to the word that should be
used for the drop cap.

.. warning::
    This only works with the PDF and Printable output if the font is scalable. ``bookman`` is the default font family
    and it has worked during our testing. If you pick a different font family, your results may vary.

    For EPub and HTML output, the first letter is wrapped in a span with a ``dropcap`` class. The default ``epub.css``
    and ``header.html`` templates provide styling, but you may want to make adjustments.

Includes
--------

QuickTome supports including other files via the ``.. include::`` directive. See :ref:`content-includes`.

Index
-----

.. important::
    Indexing is experimental and currently only supported for the ``pdf`` output format. Plans to support other output
    formats are pending as of this writing.

    Also, additional processing is required before the output may be generated, which will slow the overall build
    process. Index generation must be explicitly declared on the command line when using the ``build`` sub-command, for
    example: ``quicktome build pdf --index``

To add a term to the index, you may use the ``.. index::`` directive somewhere near the text to be indexed. For example:

.. code-block:: rst

    .. index:: project initialization

    To create a new project, run the ``quicktome init`` sub-command. You will be prompted for input unless the ``-e``
    switch is provided.

Inline indexing is also supported using the ``\index`` LaTeX command. For example:

.. code-block:: text

    To create a new project\index{project initialization}, run the ``quicktome init`` sub-command. You will be prompted
    for input unless the ``-e`` switch is provided.

Indexing occurs on a per-page basis, which sometimes results in the page number of the reference being a little off.
You may use the ``.. break::`` directive to better control paging, which may help improve the accuracy of the index.
Multiple index entries of the same name are captured for each page on which they appear.

.. important::
    Indexed terms appear at the end of the document under a level 2 heading for each letter discovered. You should add
    an ``indices.markdown`` (or similar) to the end of your loading with ``# Indices`` (or similar) if you wish to
    establish a header for this area of the document. QuickTome does **not** do this for you.

Multiple Columns
----------------

For PDF output, you may divide a section of text into multiple columns.

.. code-block:: text

    # Multi-Column Example

    .. columns:: 2

    This text will be divided into two columns.

    .. columns-end::

Any number of columns is supported, but 2 to 4 is probably the practical range for a standard 8.5x11 page. Note that
certain elements (such as tables) may not fit well within a given column's space. Some experimentation may be required.

Page Breaks
-----------

Insert page breaks using the ``.. break::`` directive.

Quotations
----------

A quote in Markdown is simply proceeded by a ``>``.

.. code-block:: text

    > Note: This is something notable.

Replacement Text
----------------

Sometimes we want to replace short abbreviations for full text to save some (or a lot of) typing.

To specify text to be replaced.

.. code-block:: text

    |QT| is a tool for managing content text files and generating output in various formats.

Then create a file at ``source/settings/replace.cfg``:

.. code-block:: cfg

    QT = QuickTome

Links are also supported:

.. code-block:: cfg

    QT = [QuickTome](https://jumpstarthq.com/quicktome)

Additionally, anchors may be referred to like so:

.. code-block:: text

    See the /example-anchor/ section.

And in the configuration file:

.. code-block:: cfg

    example-anchor = Examples

.. note::
    It is not currently possible to simultaneously support PDF links and EPub/HTML. The work around is to include a
    different anchor for both. For example, ``.. example-anchor-abbr:: EXAMP`` and ``.. example-anchor:: example``.
    The replacement text remains the same.

Spaces
------

Sometimes you want to force a little extra space between elements on the page.

.. code-block:: text

    Here is some text that is other wise too close to the next line.

    .. space::

    The next line is now further away.

The space directive is somewhat of a hack. It simply forces the insertion of an extra paragraph into the output stream.
For EPub and HTML output this results in ``<p>&nbsp;&nbsp;</p>``. However, the extra space is also applied to PDF
output.

Standout Text
-------------

Standout text is a simple way to draw attention to a passage without invoking `Annotations`_.

.. important::
    With PDF and Printable output, LaTeX is used to format the text so special characters and Markdown may cause issues
    on output. EPub and HTML are enclosed in a ``<p>`` tag with a ``standout`` class. See `Caveats`_.

Sometimes you may wish to draw attention to a particular line or paragraph. To do this, check out the ``standout``
directive.

.. code-block:: text

    .. standout::This text will appear larger, bolder, and centered. Make it count!

Note that, unlike most other directives, the ``standout`` command must be included directly before the text to be
modified. Additionally, the text must appear on a single line.

Style
-----

The style directive provides the ability to add external styling over a section of content.

.. code-block:: text

    .. style:: gray

    This text will appear gray in the output.

    .. style-end:: gray

``style-end`` is required.

For EPub and HTML output, a ``div`` with the class of ``gray`` will wrap the content. You'll need to add styling in
``epub/stylesheet.css`` and ``html/header.html`` respectively.

For PDF output, a LaTeX environment is used. This environment must be defined in the ``pdf/default.tex`` or
``printable/default.tex`` files.

.. code-block:: latex

    \newenvironment{gray}{
        \color{Gray}
    }

This makes use of the ``xcolor`` package, which, in the default templates, is conditionally loaded. In addition to
adding ``\usepackage{xcolor}`` to the preamble, you may need to remove the conditional loading statement.

See `Caveats`_.

Tables
------

QuickTome supports tables via the ``.. table::`` directive. See :ref:`content-data`.

Todos
-----

If you'd like to track todos within your content, you may use the special ``.. todo::`` directive.

.. code-block:: rst

    .. todo:: Revise the section on nematodes to include the latest estimates of the total population.

Todo content may be used anywhere in your content, and must be provided as a single line of text. Todo content is
removed prior to output.

You may list the todos you've described using the ``quicktome todos`` sub-command.

Watermarks
----------

Specify watermark text for the current page:

.. code-block:: rst

    .. watermark:: Draft

Watermark also supports a number of options:

- ``angle``: An integer representing the rotation of the text. Default: ``0``.
- ``opacity``:
- ``placement``: ``top``, ``middle``, ``bottom``.
- ``scale``: Default: ``1``

.. code-block:: rst

    .. watermark:: Draft, angle=0, color=yellow, opacity=45, placement=top

.. note::
    Background images and watermarks are mutually exclusive for the same page.

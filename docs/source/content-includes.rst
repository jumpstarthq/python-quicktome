.. _content-includes:

********
Includes
********

Includes allow content from other files to be included in the current file.

Location of Includes
====================

The include tactic works with all of the QuickTome loaders. Include files may exist anywhere in your content structure.

Defining Includes in Content
============================

QuickTome provides a mechanism for included content that is identical to that of reStructuredText:

.. code-block:: text

    .. include:: path/to/include-file.markdown

The include may be path or file name, and is always reckoned as relative to the file in which the include statement is
provided.

Additionally the extension may be omitted:

.. code-block:: text

    .. include:: include-file

Usefulness of Includes
======================

Includes are useful when you'd like to break up a large body of content into smaller, more manageable chunks.

For example, consider a handbook that details strategies for multi-tenant software:

.. code-block:: text

    multi-tenant-software-book/
    `-- source
        `-- content
            |-- 1-introduction.markdown
            |-- 2-techniques.markdown
            |-- 3-implementation.markdown
            |-- 4-resources.markdown
            `-- 5-conclusion.markdown

The ``2-techniques.markdown`` and ``3-implementation.markdown`` files might include a lot of content, so in each of
these, you might break up the content into smaller files.

The revised structure might be:

.. code-block:: text

    multi-tenant-software-book/
    `-- source
        `-- content
            |-- 1-introduction.markdown
            |-- 2-techniques
            |   |-- 1-identifiers.markdown
            |   |-- 2-namespacing.markdown
            |   |-- 3-virtualization.markdown
            |   `-- index.markdown
            |-- 3-implementation
            |   |-- 1-identifiers.markdown
            |   |-- 2-namespacing.markdown
            |   |-- 3-virtualization.markdown
            |   `-- index.markdown
            |-- 4-resources.markdown
            `-- 5-conclusion.markdown

And in the ``index.markdown`` files you would add your include statements. For example:

.. code-block:: text

    # Techniques

    .. include:: 1-identifiers.markdown

    .. include:: 2-namespacing.markdown

    .. include:: 3-virtualization.markdown

You might also consider including the headings in the index file as well, which makes it easier to maintain the document
outline. You might also add page breaks.

.. code-block:: text

    # Techniques

    .. break::

    ## Identifiers

    .. include:: 1-identifiers.markdown

    .. break::

    ## Name Spacing

    .. include:: 2-namespacing.markdown

    .. break::

    ## Virtualization

    .. include:: 3-virtualization.markdown

Assuming the manifest loader is used, the ``MANIFEST.txt`` need only refer to the files that include other files. For
example:

.. code-block:: text

    1-introduction
    2-techniques/index
    3-implementation/index
    4-resources
    5-conclusion

.. _content-hooks:

*****
Hooks
*****

.. warning::
    Hooks are an advanced topic requiring familiarity with Python and some familiarity with the QuickTome library.
    see the developer :doc:`developer-reference`. Additionally, there is currently very little error checking and no
    logging available.

Hooks are Python scripts that allow additional customization of the build.

Location of Hooks
=================

Hook scripts are located in the ``source/hooks/`` directory. Each file *must* be executable.

.. note::
    All scripts are executed from the project's root directory.

Using Hooks
===========

Two hooks are currently recognized:

- `Before Load Content`_
- `Before Export`_
- `After Export`_

Before Load Content
-------------------

The before load hook is executed before the input file is generated and before any other processing has occurred.
You may use this for things like:

- Create or process images before they are copied to the ``temporary/`` (staging) directory.
- Creating new content files to be included in the output.

The script is located at ``source/hooks/before_load_content.py``.

The following example is an excerpt from the hook provided in the ``process`` skeleton, which parses and INI file to
create a Markdown file that will later be combined into the output.

.. code-block:: python

    #! /usr/bin/env python

    from myninjas.settings import INIConfig
    from myninjas.utils import write_file
    import os

    # Create the roles.markdown file from the roles.ini file.
    config = INIConfig(os.path.join("source", "content", "_data", "roles.ini"))
    config.load()

    lines = list()
    lines.append("# Roles")
    lines.append("")

    for section in config.get_sections():
        lines.append("## %s" % section.title)
        lines.append("")
        lines.append(section.description)
        lines.append("")

    path = os.path.join("source", "content", "roles.markdown")
    write_file(path, "\n".join(lines))

This creates the ``roles.markdown`` file for inclusion in the final output.

.. note::
    The ``myninjas`` library is a requirement and is installed automatically when QuickTome is installed.

Before Export
-------------

The before export hook runs just prior to the ``pandoc`` command. You may use this to do things like:

- Create or copy assets to the staging directory.
- Append or prepend content to the input file.
- Dynamically creating templates.

After Export
------------

The after export hook is executed after the ``pandoc`` command. You may use this for things like:

- Move or copy output files.
- Perform any desired cleanup steps.

The script is located at ``source/hooks/after_export.py``.

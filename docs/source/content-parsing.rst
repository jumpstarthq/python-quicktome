.. _content-parsing:

***************
Parsing Content
***************

By default, all content files are parsed as a Jinja2 template. This allows for a number of special tactics within your
content.

.. tip::
    If you do not wish to parse your content files as templates, use the ``-N`` (``--no-context``) switch with the
    ``build`` sub-command.

Known Limitations
=================

There is currently one known limitation with parsing content files as templates, and that is the use of the
`Header Identifiers`_. Since Jinja treats the ``{#`` as the beginning of a comment, using Pandoc's Markdown for header
identifiers will result in an error.

.. _Header Identifiers: https://pandoc.org/MANUAL.html#header-identifiers

The workaround is to enclose the statement in a literal print statement.

.. code-block:: text

    ### My Specific Heading {{ "{#specific}" }}

The ``raw`` block could be used, but is less pretty:

.. code-block:: text

    ### My Specific Heading {% raw %}{#specific}{% endraw %}

Of course, you can disable template processing, but if you find it beneficial, this approach allows you to also use
header identifiers.

.. _content-directives-variables:

Variables
=========

Variables allow dynamic content to be populated before input is passed to Pandoc. Variables may be used in your content
files like ``{{ variable_name }}``.

.. code-block:: text

    ## Notable Differences in this Revised Edition

    In the ``{{ previous_project.edition }}`` edition, all examples were presented without reference to external
    studies, but in the ``{{ project.edition }}`` edition, we've added citations for quicker and easier reference.

The Source of Variables
-----------------------

Variables may come from various sources, described below.

Current Variables
.................

A number of variables are supplied which may be used in your content files.

- ``current_month``: The current month (integer).
- ``current_month_name``: The name of the current month.
- ``current_time``: The current time as a Python datetime object.
- ``current_year``: The current year (integer).
- ``current_version``: Loaded from the project's ``VERSION.txt`` file.
- ``output_format``: For the ``build`` sub-command, the output format will be one of ``docx``, ``epub``, ``html``, ``mobi``, ``pdf``, ``printable`` or ``slideshow``.

Settings in Context
...................

All settings from the ``meta.ini`` file are added to the context as individual section objects. These include, but may
not be limited to:

- ``author``: This object includes ``name`` and ``title`` attributes.
- ``copyright``: This object includes ``name`` and ``year`` attributes.
- ``project``: This object includes ``title``, ``description``, and ``edition`` attributes.

You may use this in a template like ``{{ author.name }}``.

You may also supply additional attributes, for example:

.. code-block:: ini

    [author]
    name = Wile E. Coyote
    title = Carnivorus Vulgaris
    email = coyote@carnivousvulgaris.com

This would make ``{{ author.email }}`` available in the context.

.. code-block:: text

    Please do not hesitate to contact me at {{ author.email }}.

Variables Added from the Command Line
.....................................

Variables added using the ``-V`` switch are also included in the context.

.. code-block:: bash

    quicktome build pdf -V ruleset:advanced;

This may be used like any other variable, e.g. ``{{ ruleset }}``, but may also be used for `Conditional Statements`_.

The Extra Variables File
........................

Additional variables may be added to the context by providing the ``-E`` switch with a path to an INI file containing
the extra variables. See :ref:`settings`.

Conditional Statements
======================

It is possible to process content based on conditional statements. For example, imagine you are creating a rule book for
a game and some rules are only to be included in the "advanced" version of the rule book.

.. code-block:: text

    {% if ruleset == "advanced" %}

    .. include:: advanced-rules

    {% else %}

    .. include:: basic-rules

    {% endif %}

From the command line, you may build the advanced version using the ``-V`` switch:

.. code-block:: bash

    quicktome build pdf --prefix=Advanced -V ruleset:advanced;

This would cause the advanced rules to be generated.

.. tip::
    This would be easily automated by adding and ``advanced`` target to the ``Makefile``.

Other Template Controls
=======================

See the `Jinja template designer documentation`_ for more detailed information.

.. _Jinja template designer documentation: http://jinja.pocoo.org/docs/2.10/templates/

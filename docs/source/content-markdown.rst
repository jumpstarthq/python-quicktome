.. _content-markdown:

********
Markdown
********

This guide may be used as a quick reference for `Markdown`_. Please refer `Pandoc's Markdown`_ guide for a full reference.

.. _Markdown: https://daringfireball.net/projects/markdown/
.. _Pandoc's Markdown: https://pandoc.org/MANUAL.html#pandocs-markdown

Code Highlight
==============

Code fencing is supported by Pandoc:

.. code-block:: text

    ```python
    for i in items:
        print(i)
    ```

To add line numbers:

.. code-block:: text

    ``` {.python .numberLines}
    for i in items:
        print(i)
    ```

To control the theme used to highlight code: ``quicktome build pdf --highlight-style=espresso``

.. tip::
    To see available styles, use ``pandoc --highlight-styles``.

Footnotes
=========

Create auto-numbered footnotes like so:

.. code-block:: text

    The *Live* [^choice_of_live] environment is a tested, stable, and approved version that may be available and in use by
    Customers or Users.

    [^choice_of_live]: We use the word *live* instead of *production* to avoid confusion. Whereas *production* may indicate that something is in active use or currently being produced, *live* always means the project is in use (or is available for use).

Links
=====

TODO

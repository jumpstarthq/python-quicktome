.. _resources:

*********
Resources
*********

Alternative Products
====================

The following products may be considered as an alternative to QuickTome:

- `Bookdown`_

.. _Bookdown: https://bookdown.org

Editors
=======

There are a large number of editors for Markdown. As of this writing, it seems like there is a new Markdown editor
released every month.

We've found the following features useful when working with projects of various types:

- Command Line: It can be nice to open files from the command line using the editor.
- Directories: It's pretty handy when the editor can open multiple files at once from a selected directory. Even
  better is when the editor can display those files for easy selection.
- Export: Although the purpose of QuickTome is to compile and export content to various formats, exporting individual
  files may be useful in some cases.
- Outline: An outline of the current document is extremely convenient for review and editing. This serves as a live
  table of contents for the document and makes navigation much easier.
- Split Screen: Having the same view of the current document is often a useful feature to allow viewing different areas
  of the file.

Another feature, not considered above, (but worth considering) is whether the editor wraps long lines of text in order
to see the whole line, or if an explicit carriage return is required. (Explicit returns are generally undesirable from a
processing standpoint.) Of the editors in the table below, only PyCharm requires carriage returns for line wrapping.

.. csv-table:: Editor Overview
    :file: _data/editors.csv

.. note::
    This list is woefully incomplete. The entries here stood out to us for one reason or another. Since we are a
    development shop primarily using Apple workstations, the editors here may be somewhat slanted for Mac.

**Who wins?** This is largely a matter of preference. As we spend a lot of time in PyCharm, we also use it quite a bit
for editing content. We reviewed dozens of editors when creating this resources page, and found `Typora`_ (in beta as of
this writing) to be the nicest and most compatible for Pandoc's Markdown. (This is because Typora uses Pandoc behind the
scenes as its export engine, hence the compatibility.)

.. _Typora: https://typora.io

Documentation
=============

- `LaTeX`_
- `Pandoc`_

.. _LaTeX: https://www.latex-project.org
.. _Pandoc: https://pandoc.org/MANUAL.html

Support
=======

- `LaTeX Stack Exchange`_: We've spent many long hours on this question and answer site during the development of
  QuickTome. It is an excellent resource if one needs to use LaTeX for anything.

.. _LaTeX Stack Exchange: https://tex.stackexchange.com

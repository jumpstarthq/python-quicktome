.. _output:

******
Output
******

QuickTome uses your content and Pandoc to generate output in various formats:

Output Formats
==============

The supported output formats are:

- docx: Output format for MS Word documents.
- epub: Output format for epub (for example, Apple iBook) consumption.
- html: Output format for a standalone HTML file.
- mobi: Output format for mobi (for example, Kindle) consumption.
- pdf: Output format for a PDF file.
- print: Output format for a print-ready PDF file.
- slideshow: Output format for slideshow HTML.

.. important::
    The ``mobi`` output first requires that an ``epub`` file be generated.

.. warning::
    The ``slideshow`` format is not yet fully implemented.

Using the Build Command
=======================

You may use the ``build`` sub-command to generate output. For example, to generate a PDF file:

.. code-block:: bash

    quicktome build pdf;

Use ``quicktome build --help`` for more options.

Using the Makefile
==================

The ``init`` sub-command creates a ``Makefile`` with pre-defined targets for all of the output formats. You may
customize these or create new targets as needed.

Assuming ``make`` is installed on your system, you could generate a PDF file using the following command:

.. code-block:: bash

    make pdf;

You may also generate all output using ``make all``.

Using Templates
===============

Templates control or impact the output of specific output formats. Detailed information on these may be found in the
`Pandoc documentation`_ where their use is described among the command line switches.

.. _Pandoc documentation: https://pandoc.org/MANUAL.html

When the ``init`` sub-command is used, a number of templates are created by default:

- ``docx/reference.docx`` is used by the ``docx`` output format to provide style hints for creating MS Word documents.
- ``epub/stylesheet.css`` is used by the ``epub`` output format to style the output. The ``cover.jpg`` is also required,
  but not provided, if a cover is desired. See :ref:`content-cover`.
- ``html/footer.html`` is added to the bottom of the standalone HTML file created by the ``html`` output format.
- ``html/header.html`` may be used to add style to the standalone HTML file created by the ``html`` output format.
- ``pdf/default.tex`` is a LaTeX used by the ``pdf`` output format.
- ``printable/default.tex`` is a LaTeX used by the ``print`` output format.

.. note::
    LaTeX is a format that is not for the faint of heart. The default templates will probably be *okay*, but sometimes
    editing is required to produce a specific result. See the `Official LaTeX documentation`_ for more information.

    The `LaTeX Stack Exchange`_ is a good resource for asking questions *after* you've done some initial legwork.

.. _LaTeX Stack Exchange: https://tex.stackexchange.com
.. _Official LaTeX documentation: https://www.latex-project.org
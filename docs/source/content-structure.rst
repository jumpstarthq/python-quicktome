.. _content-structure:

*********
Structure
*********

The Content Path
================

All content for the project is stored in the ``source/content/`` directory. With QuickTome, *content* refers to
Markdown, images, or data files.

Organizational Options
======================

Tree Structure
--------------

The Tree Structure option uses the files and directories it finds in ``source/content/`` to create the input file used
by Pandoc for export. Since the files are combined, the order in which they are found is important. This is
accomplished by naming files and/or directories with a number such as ``01``, ``02``, ``03`` and so on. The leading
``0`` is required for proper sorting of single-digit numbers.

Since the other options require additional setup, *the Tree Structure is the default* when running the ``init``
sub-command.

.. tip::
    Although your choice for structuring a project is largely a matter of preference, large projects will
    become cumbersome to maintain when relying on the file system for organization.

Using OPML
----------

QuickTome supports using OPML for specifying (or automatically finding) content files. This *does* require the OPML to
be in a pre-defined format, though the ``init`` sub-command will copy a couple of sample files for you.

.. figure:: _static/images/omnioutliner-screen-shot.jpg

    The Default OmniOutliner File

To locate files using the outline, it first checks to see if the ``file`` column has a value. Next, it creates a
"slugified" version of the topic title and looks for that file. The slug is normalized by converting the title to lower
case, removing special characters, and using underscores for word separation.

Consider the following example titles:

- Introduction
- War, What's It Good For?
- Part 1: War and Peace
- 1st Chapter - War
- 2nd Chapter - Peace

The slugified versions -- and therefore the require names for the corresponding files -- would be:

- introduction
- war_whats_it_good_for
- part_1_war_and_peace
- 1st_chapter_war
- 2nd_chapter_peace

.. important::
    Using the ``file`` column is required if a topic title (that corresponds to a file) is duplicated in the outline.

Using an outline such as this may be convenient for organizing your thoughts, and it can be extremely convenient for
large projects since the outline automatically controls the order in which the content files are compiled.

.. important::
    If you are using an OmniOutliner file, you *must* export it to ``outline.opml`` before running the
    ``build`` sub-command.

On the other hand, using OPML requires additional software and may impose an extra step (as with the OmniOutliner
export) prior to running the build.

Manifest Approach
-----------------

With the Manifest Approach, the files to be loaded are listed in the ``source/MANIFEST.txt`` file. For example:

.. code-block:: text

    title
    contact
    legal
    introduction

    chapters/1
    chapters/2
    chapters/3
    chapters/4
    chapters/5

    conclusion
    # glossary

Notice that the extension may be omitted. Also note that blank lines and lines beginning with ``#`` are skipped.

The manifest is a simple and more flexible alternative to using the `Tree Structure`_ and doesn't require any special
software as with `Using OPML`_. There is also an additional benefit: QuickTome may be used to generate the project
structure using the ``MANIFEST.txt`` as a guide. For example:

.. code-block:: bash

    quicktome skeleton manifest;

This command may be executed over and over, and each time it will create the structure it finds in the file.

.. _content-structure-images:

Organizing Images
=================

Images may exist anywhere in the structure of your content provided they are always contained within an ``_images/``
directory.

The underscore is used to visually distinguish the directory from other directories. You may change this, as well as
control which files are copied for output using the ``images`` section of your project's ``meta.ini`` file.

.. code-block:: ini

    [images]
    directory = images
    extensions = .jpg .png

When images are copied to the staging directory for output, they are all re-homed in the ``_images/`` sub-directory,
making all image paths specified as ``_images/name.ext`` valid.

.. include:: content-skeletons.rst

BIBLIOGRAPHY_EXTENSIONS = [
    ".bib",
    ".bibtex",
    ".copac",
    ".json",
    ".yaml",
    ".enl",
    ".xml",
    ".wos",
    ".medline",
    ".mods",
    ".ris",
]
"""A list of bibliography extensions recognized by Pandoc. See https://pandoc.org/MANUAL.html#citations"""

MARKDOWN_EXTENSIONS = [
    ".markdown",
    ".md",
    ".mkd",
    ".txt",
]
"""A list of recognized Markdown extensions. See https://superuser.com/a/285878/180333"""

INPUT_FORMAT_MD = "markdown"
"""Used to indicate that input (content files) are in Markdown format. This is passed to pandoc with the -f switch."""

# INPUT_FORMAT_RST = "reStructuredText"
# """Used to indicate that input (content files) are in reStructuredText format."""
#
# INPUT_FORMAT_CHOICES = [
#     INPUT_FORMAT_MD,
#     INPUT_FORMAT_RST,
# ]
# """Input format choices are used during project init."""
#
# INPUT_FORMAT_MAPPING = {
#     INPUT_FORMAT_MD: ('.markdown', '.md', '.mkd', '.txt'),
#     INPUT_FORMAT_RST: ('.rst', '.txt'),
# }
# """Map input formats to possible file extensions."""
#
# """
#
# .markdown
# .mdown
# .mkdn
# .md
# .mkd
# .mdwn
# .mdtxt
# .mdtext
# .text
# .Rmd
# """

INPUT_LOADER_MANIFEST = "MANIFEST.txt"
"""The manfiest defines the files to be included in the final output."""

INPUT_LOADER_OUTLINE = "outline.opml"
"""The OPML file defines the files to be included in the final output."""

INPUT_LOADER_TREE = "tree"
"""The tree loader loads files to be included in the final output from the file system."""

INPUT_LOADER_CHOICES = [
    INPUT_LOADER_MANIFEST,
    INPUT_LOADER_OUTLINE,
    INPUT_LOADER_TREE,
]
"""Input loader choices are used during project init."""

OUTPUT_DOCX = "docx"
"""Output format for MS Word documents."""

OUTPUT_EPUB = "epub"
"""Output format for epub (for example, Apple iBook) consumption."""

OUTPUT_HTML = "html"
"""Output format for a standalone HTML file."""

OUTPUT_MOBI = "mobi"
"""Output format for mobi (for example, Kindle) consumption."""

OUTPUT_PDF = "pdf"
"""Output format for a PDF file."""

OUTPUT_PRINTABLE = "printable"
"""Output format for a print-ready PDF file."""

OUTPUT_SLIDESHOW = "slideshow"
"""Output format for slideshow HTML."""

OUTPUT_CHOICES = [
    OUTPUT_DOCX,
    OUTPUT_EPUB,
    OUTPUT_HTML,
    OUTPUT_MOBI,
    OUTPUT_PDF,
    OUTPUT_PRINTABLE,
    OUTPUT_SLIDESHOW,
]
"""Output choices are used by the build sub-command."""

PAGE_EXTENSIONS = [
    ".htm",
    ".html",
    ".markdown",
    ".md",
    ".rst",
    ".txt",
]
"""A list of extensions (including the dot) that are recognized as containing page content."""

SKELETON_CHOICES = {
    "manifest": "Use an existing MANIFEST.txt file to build project structure.",
    "novel": "A typical structure for a novel.",
    "paper": "A structure of a technical paper or similar work.",
    "process": "Process documentation.",
    "software": "A structure that may be used for software documentation.",
}

STATIC_EXTENSIONS = [
    ".gif",
    ".jpg",
    ".jpeg",
    ".png",
    ".svg",
    ".tiff",
]
"""A list of extensions (including the dot) that are recognized as static content."""

# TODO: Check STATIC_EXTENSIONS against those that may be included in pandoc output.

# Imports

import logging
from myninjas.utils import write_file
import os
# noinspection PyPackageRequirements
from slugify import slugify
from ..constants import OUTPUT_SLIDESHOW
from ..variables import LOGGER_NAME
from .outputs import MAPPING as OUTPUT_MAPPING

log = logging.getLogger(LOGGER_NAME)

# Exports

__all__ = (
    "Staging",
)

# Classes


class Staging(object):
    """Staging represents the pre-conversion resources required by Pandoc."""

    def __init__(self, config, loader, project, directory="temporary", extension=".markdown", preview=False):
        self.config = config
        self.directory = directory
        self.extension = extension
        self.loader = loader
        self.preview = preview
        self.project = project
        self.root = os.path.join(project.root, directory)
        self._image_path = os.path.join(self.root, "_images")
        self._template_path = os.path.join(self.root, "templates")

    def collect_content_files(self):
        """Collect (combine) content files into a single string that may be used to write the input file.

        ;:rtype: str

        """
        a = list()
        for f in self.loader:
            log.debug("Processing: %s" % f.path)
            f.load()

            a.append(f.content)
            a.append("")

        content = "\n".join(a)

        replacements = self.loader.get_replacement_text()
        if replacements is not None:
            for key, value in replacements.items():
                content = content.replace("/%s/" % key, r"\hyperlink{%s}{%s}" % (key, value))
                content = content.replace("|%s|" % key, value)

        return content

    def collect_slideshow(self):
        """Collect slideshow content into a single string that may be written to the input file.

        :rtype: str

        """
        a = list()
        a.append("%% %s" % self.config.project.title)
        a.append("")

        for f in self.loader:
            log.debug("Processing for slideshow content: %s" % f.path)
            f.load()

            b = f.get_slideshow()
            if len(b) > 0:
                a += b

        if len(a) == 0:
            return ""

        return "\n".join(a)

    def copy_bibliography_file(self):
        """Copy the bibliography file, if it exists, to the staging directory.

        :rtype: int

        """
        bib = self.loader.get_bibliography_file()
        if bib.exists:
            log.info("Copying bibliography file: %s" % bib.path)
            if self.preview:
                return 1

            to_path = os.path.join(self.root, bib.basename)
            success, error = bib.copy(to_path)

            if success:
                return 1
            else:
                log.debug("Failed to copy bibliography file: %s" % error)

        return 0

    def copy_image_files(self):
        """Copy image files to the staging directory.

        :rtype: int
        :returns: The number of files copied.

        """
        log.info("Copying image files to staging directory.")

        count = 0
        for f in self.loader.get_image_files():
            to_path = os.path.join(self._image_path, os.path.basename(f.path))
            log.debug("Copy %s image to %s" % (f.path, to_path))

            if self.preview:
                count += 1
            else:
                success, error = f.copy(to_path)
                if success:
                    count += 1
                else:
                    log.debug("Failed to copy image file: %s" % error)

        return count

    def copy_template_files(self):
        """Copy template files to the staging directory.

        :rtype: int
        :returns: The number of files copied.

        """
        log.info("Copying template files to staging directory.")

        count = 0
        for f in self.loader.get_template_files():
            to_path = os.path.join(self._template_path, os.path.basename(f.directory), f.basename)
            log.debug("Copy %s template to %s" % (f.basename, to_path))

            if self.preview:
                count += 1
            else:
                success, error = f.copy(to_path)
                if success:
                    count += 1
                else:
                    log.debug("Failed to copy template file: %s" % error)

        return count

    def prepare_input(self, output_format):
        """Prepare input files  for use in Pandoc conversion.

        :param output_format: The output format to be prepared.
        :type output_format: str

        """
        # Copy image files to the staging directory.
        self.copy_image_files()

        # Copy the bibliography file (if it exists) to the staging directory.
        self.copy_bibliography_file()

        # Collect and combined content files and write a single file to the staging directory.
        if output_format == OUTPUT_SLIDESHOW:
            content = self.collect_slideshow()
            name = "%s-Slideshow.markdown" % slugify(self.config.project.title)
        else:
            content = self.collect_content_files()
            name = None

        self.write_input_file(content, name=name)

    def prepare_output(self, output_format, **kwargs):
        """Prepare the staging environment for use by Pandoc.

        :param output_format: The output format to be prepared.
        :type output_format: str

        :rtype: BaseType[BaseOutput]
        :returns: Returns an output instance.

        Keyword arguments are passed to the output class.

        """
        # Make sure the staging (temporary) directory exists.
        self.project.create_path("temporary")

        # Make sure the output directory exists.
        self.project.create_path("output")

        # Copy output templates.
        count = self.copy_template_files()
        log.debug("Copied %s template files." % count)

        # Instantiate the output.
        output_class = OUTPUT_MAPPING[output_format]
        output = output_class(self.config, self.project, self, **kwargs)

        # Allow the output to do its own preparation work.
        if not self.preview:
            output.prepare()

        # Ready to rock and roll. Return the output instance.
        return output

    def write_input_file(self, content, name=None):
        """Write the input file to the staging directory."""

        if name is None:
            name = slugify(self.config.project.title) + ".markdown"

        path = os.path.join(self.root, name)
        log.info("Writing input file: %s" % path)

        if not self.preview:
            write_file(path, content=content)

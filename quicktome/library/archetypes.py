"""
An *archetype* is a pre-defined model for content that may be used to create new content files.

.. note::
    We selected the word *archetype* after seeing its use in the Hugo static site generator. This is a better term than
    *template*, since template may have other uses.

An example archetype file for a new chapter of a book might be as follows:

.. code-block:: text

    # Chapter Title

    Write your chapter content here.

But the power of archetypes are revealed when the source file may be parsed as a template:

.. code-block:: text

    # {% if number %}{{ number }}. {% endif %}{{ title }}

    {% if include_lorem %}
    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada
    erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.
    {% else %}
    Write your chapter content here.
    {% endif %}

To allow prompting the user for template context, you should also create a meta file for the archetype.

.. code-block:: ini

    ; chapter.ini (the meta file name should be the same as that of the archetype file)
    [include_lorem]
    default = yes
    label = Include Lorem
    type = boolean

    [number]
    label = Chapter Number
    required = no
    type = integer

    [title]
    label = Chapter Title
    required = yes
    type = string

"""
# Imports

from myninjas import prompt
from myninjas.feedback import Feedback
from myninjas.feedback import GREEN, RED, colorize
from myninjas.settings import INIConfig
from myninjas.utils import indent, parse_jinja_template, read_file, smart_cast, write_file, File
import os
import warnings
from ..variables import QUICKTOME_PATH

# Exports

__all__ = (
    "Archetype",
    "Member",
    "Skeleton",
)

# Classes


class Archetype(File):
    """Represents a pre-defined content model."""

    def copy(self, path, context=None):
        """Copy the archetype file to the given path.

        :param path: The path to the new file.
        :type path: str

        :param context: If given, the archetype source will first be parsed as a Jinja template.
        :type context: dict

        """
        if context is not None:
            content = parse_jinja_template(self.path, context)
        else:
            content = read_file(self.path)

        write_file(path, content=content, make_directories=True)

    def get_form(self):
        """Get the input form for new content using the archetype's meta.

        :rtype: prompt.Form | None

        """
        if not self.has_meta:
            return None

        class ArchetypeForm(prompt.Form):
            pass

        form = ArchetypeForm()

        inputs = self.get_meta()
        for i in inputs.get_sections():
            field_name = i.get_name()
            label = i.get("label", field_name).replace("_", " ").title()

            default = None
            if i.has("default"):
                default = i.default

            if i.has("required"):
                required = smart_cast(i.required)
            else:
                required = False

            if i.type == "boolean":
                # noinspection PyUnresolvedReferences
                form.fields[field_name] = prompt.Boolean(label, default=default, required=required)
            elif i.type == "integer":
                # noinspection PyUnresolvedReferences
                form.fields[field_name] = prompt.Integer(label, default=default, required=required)
            else:
                # noinspection PyUnresolvedReferences
                form.fields[field_name] = prompt.String(label, default=default, required=required)

        return form

    def get_meta(self):
        """Get the meta data that may be used to prompt the user for template context.

        :rtype: Config | None

        """
        if not self.has_meta:
            return None

        path = os.path.join(self.directory, "%s.ini" % self.name)
        config = INIConfig(path)

        with warnings.catch_warnings():
            warnings.simplefilter("ignore")

            config.load()

        return config

    @property
    def has_meta(self):
        """Indicates a meta data file has been defined for the archetype.

        :rtype: bool

        """
        path = os.path.join(self.directory, "%s.ini" % self.name)
        return os.path.exists(path)


class Member(object):
    """A member is a file or directory in a skeletal structure."""

    def __init__(self, from_path, to_path, level=0):
        """Initialize the member.

        :param from_path: The path from which the member will be created or copied.
        :type from_path: str

        :param to_path: The path to which the member will be created or copied in the project.
        :type to_path: str

        :param level: The level of the member in the directory structure.
        :type level: int

        """
        self.from_path = from_path
        self.is_directory = os.path.isdir(from_path)
        self.is_file = os.path.isfile(from_path)
        self.level = level
        self.to_path = to_path
        self.to_path_exists = os.path.exists(to_path)

    def copy(self):
        """Copy the member to the project.

        :rtype: bool

        """
        if self.to_path_exists:
            return False

        if self.is_directory:
            os.makedirs(self.to_path)
        else:
            content = read_file(self.from_path)
            write_file(self.to_path, content=content, make_directories=True)

        return True

    def to_string(self, color=False):
        """Export the member as an indented and optionally colorized string.

        :param color: Use color or not. Members that exist in the project are green and members that do not exist are
                      red.
        :type color: bool

        """
        i = " " * self.level * 4
        if self.is_directory:
            output = "{}{}/".format(i, os.path.basename(self.from_path))
        else:
            output = "{}{}".format(i, os.path.basename(self.from_path))

        if color:
            if self.to_path_exists:
                output = colorize(GREEN, output)
            else:
                output = colorize(RED, output)

        return output


class Skeleton(object):
    """A skeleton is a pre-defined tree structure for a project or portion of a project."""

    def __init__(self, name, description=None, path=None, project=None):
        """Initialize a skeleton.

        :param name: The name of the skeleton.
        :type name: str

        :param description: A brief description of the skeleton.
        :type description: str

        :param path: The path to the skeleton.
        :type path: str

        :param project: The current project instance to which the structure will be applied.
        :type project: Project

        """
        self.description = description
        self.name = name
        self.path = path or os.path.join(QUICKTOME_PATH, "skeletons", name)
        self.project = project

    @property
    def exists(self):
        """Indicates whether the skeleton exists.

        :rtype: bool

        """
        return os.path.exists(self.path)

    def get_structure(self):
        """Get the structure of the skeleton.

        :rtype: list[Member] | None

        """
        if not self.exists:
            return None

        a = list()
        path = os.path.join(self.path, "source")
        for root, dirs, files in os.walk(path):
            files.sort()
            if root == path:
                a.append(Member(path, os.path.join(self.project.root, "source")))
                continue

            from_path = root
            to_path = os.path.join(self.project.root, "source", os.path.basename(root))
            root_level = root.replace(path, "").count(os.sep)
            member = Member(from_path, to_path, level=root_level)
            a.append(member)

            for f in files:
                from_path = os.path.join(root, f)
                to_path = os.path.join(self.project.root, "source", os.path.basename(root), f)
                file_level = root_level + 1
                member = Member(from_path, to_path, level=file_level)
                a.append(member)

        return a

    def to_console(self):
        """Export information regarding the structure to a text (and colorized) format.

        :rtype: str

        """
        a = Feedback()
        a.heading(self.name)

        if self.description is not None:
            a.plain(self.description)
            a.cr()

        members = self.get_structure()
        if members is not None:
            for m in members:
                if m.to_path_exists:
                    a.green(m.to_string())
                else:
                    a.red(m.to_string())

        return "\n".join(a.messages)

    def to_markdown(self):
        """Export information regarding the structure in Markdown format.

        :rtype: str

        """
        a = list()
        a.append("# %s" % self.name)
        a.append("")

        if self.description is not None:
            a.append(self.description)
            a.append("")

        # a.append("Source: %s" % self.path)
        # a.append("")

        members = self.get_structure()
        if members is not None:
            a.append("The structure is as follows:")
            a.append("")

            for m in members:
                a.append(indent(m.to_string()))

            a.append("")

        return "\n".join(a)

    def to_rst(self):
        """Export information regarding the structure in reStructuredText format.

        :rtype: str

        """
        a = list()
        a.append("%s" % self.name)
        a.append("-" * len(self.name))
        a.append("")

        if self.description is not None:
            a.append(self.description)
            a.append("")

        # a.append("Source: %s" % self.path)
        # a.append("")

        members = self.get_structure()
        if members is not None:
            a.append("The structure is as follows:")
            a.append("")
            a.append(".. code-block:: text")
            a.append("")

            for m in members:
                a.append(indent(m.to_string()))

            a.append("")

        return "\n".join(a)

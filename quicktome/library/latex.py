"""
Utility classes for generating Latex statements programmatically.

"""
# Exports

__all__ = (
    "escape_characters",
    "BackgroundImage",
    "Watermark",
)

# Functions


def escape_characters(content):
    """Escape special characters that exist inside of a LaTex environment.

    :param content: The content to be escaped.
    :type content: str

    :rtype: str

    See https://tex.stackexchange.com/a/34586/177860

    """
    content = content.replace("&", r"\&")
    content = content.replace("%", r"\%")
    content = content.replace("$", r"\$")
    content = content.replace("#", r"\#")
    content = content.replace("_", r"\_")
    content = content.replace("{", r"\{")
    content = content.replace("}", r"\}")
    content = content.replace("~", r"\textasciitilde")
    content = content.replace("^", r"\textasciicircum")
    content = content.replace(r"\\", r"\textbackslash")

    return content

# Classes


class BackgroundImage(object):
    """Use tex background to create a background image.

    See: http://mirror.utexas.edu/ctan/macros/latex/contrib/background/background.pdf

    See also: https://tex.stackexchange.com/a/167741/177860

    """

    def __init__(self, path, angle=0, height=None, opacity=None, placement=None, scale=1, width=None):
        """Initialize the image.

        :param path: The path to the image.
        :type path: str

        :param angle: The angle of rotation of the image.
        :type angle: int

        :param height: The height of the image in centimeters or ``full``.
        :type height: int | str

        :param opacity: The opacity of the image.
        :type opacity: int

        :param placement: The placement of the image; ``top``, ``center``, ``bottom``.
        :type placement: str

        :param scale: The scale of the image.
        :type scale: int

        :param width: The width of the image in centimeters or ``full``.
        :type width: int | str

        """
        self.angle = angle
        self.height = height
        self.opacity = opacity
        self.path = path
        self.placement = placement
        self.scale = scale
        self.width = width

    def to_tex(self):
        """Export to latex.

        :rtype: str

        """
        # Background setup.
        a = list()
        a.append("angle=%s" % self.angle)

        if self.opacity is not None:
            a.append("opacity=%s" % self.opacity)

        if self.placement is not None:
            a.append("placement=%s" % self.placement)

        a.append("scale=%s" % self.scale)

        # Include graphics.
        b = list()

        if self.height is not None:
            if self.height == "full":
                height = r"\paperheight"
            else:
                height = "%scm" % self.height

            b.append("height=%s" % height)

        if self.width is not None:
            if self.width == "full":
                width = r"\paperwidth"
            else:
                width = "%scm" % self.width

            b.append("width=%s" % width)

        contents = r"contents={\includegraphics[%s]{%s}}" % (", ".join(b), self.path)
        a.append(contents)

        c = list()
        c.append(r"\backgroundsetup{%s}" % ", ".join(a))
        c.append(r"\BgThispage")

        return "\n".join(c)


class Watermark(object):
    """Use tex background to create a watermark.

    See http://mirror.utexas.edu/ctan/macros/latex/contrib/background/background.pdf

    """

    def __init__(self, label, angle=0, color="red", opacity=None, placement=None, scale=15):
        """Initialize a watermark.

        :param angle: The angle of rotation of the text.
        :type angle: int

        :param color: The color of the text.
        :type color: str

        :param opacity: The opacity of the text.
        :type opacity: int

        :param placement: The placement of the text; ``top``, ``center``, ``bottom``.
        :type placement: str

        :param scale: The scale of the text.
        :type scale: int

        """
        self.angle = angle
        self.color = color
        self.label = label
        self.opacity = opacity
        self.placement = placement
        self.scale = scale

    def to_tex(self):
        """Export to latex.

        :rtype: str

        """
        a = list()
        a.append("angle=%s" % self.angle)

        a.append("color=%s" % self.color)

        a.append("contents={%s}" % self.label)

        if self.opacity is not None:
            a.append("opacity=%s" % self.opacity)

        if self.placement is not None:
            a.append("placement=%s" % self.placement)

        a.append("scale=%s" % self.scale)

        b = list()
        b.append(r"\backgroundsetup{%s}" % ", ".join(a))
        b.append(r"\BgThispage")

        return "\n".join(b)

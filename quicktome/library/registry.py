# Imports

from myninjas.utils import File
import os
from ..constants import MARKDOWN_EXTENSIONS, STATIC_EXTENSIONS

# Classes


class Register(object):
    """A register for content files, data files, and images across an entire project."""

    def __init__(self):
        """Initialize a registry instance."""
        self._content_files = dict()
        self._data_files = dict()
        self._image_files = dict()

    # def add(self, file, used=False):
    #     if file.extension in MARKDOWN_EXTENSIONS:
    #         self._content_files[file.path] = used
    #     elif file.extension in STATIC_EXTENSIONS:
    #         self._image_files[file.path] = used
    #     elif file.extension == ".csv":
    #         self._data_files[file.path] = used
    #     else:
    #         pass

    def find_all_files(self, path):
        """Find all files from the given path and add them to the registry."""
        for root, dirs, files in os.walk(path):
            files.sort()
            for f in files:
                file_path = os.path.join(root, f)
                file = File(file_path)

                if file.extension in MARKDOWN_EXTENSIONS:
                    self._content_files[file_path] = False
                elif file.extension == ".csv":
                    self._data_files[file_path] = False
                elif file.extension in STATIC_EXTENSIONS:
                    self._image_files[file_path] = False
                else:
                    pass

    def get_unused(self, file_type="content"):
        """Get the files that were found but never loaded or used.

        :param file_type: The type of type; ``content``, ``data``, ``image``.
        :type file_type: str

        :rtype: list[str]
        :returns: A list of file paths that were found but never usesd.

        """
        _files = list()
        if file_type == "data":
            files = list(self._data_files.keys())
            files.sort()
            for i in files:
                if self._data_files[i] is False:
                    _files.append(i)
        elif file_type == "image":
            files = list(self._image_files.keys())
            files.sort()
            for i in files:
                if self._image_files[i] is False:
                    _files.append(i)
        else:
            files = list(self._content_files.keys())
            files.sort()
            for i in files:
                if self._content_files[i] is False:
                    _files.append(i)

        return _files

    def mark_used(self, file):
        """Mark a file as used (loaded) for output.

        :param file: The file that was used.
        :type file: ContentFile | DataFile | ImaageFile

        """
        if file.extension in MARKDOWN_EXTENSIONS:
            self._content_files[file.path] = True
        elif file.extension in STATIC_EXTENSIONS:
            self._image_files[file.path] = True
        elif file.extension == ".csv":
            self._data_files[file.path] = True
        else:
            pass


registry = Register()

# Imports

import logging
from myninjas.utils import File, read_file, write_file
import opml
import os
# noinspection PyPackageRequirements
from slugify import slugify
from ..constants import OUTPUT_PRINTABLE, PAGE_EXTENSIONS, STATIC_EXTENSIONS
from ..variables import LOGGER_NAME
from .files import BibliographyFile, ContentFile, ImageFile, TemplateFile
from .registry import registry

log = logging.getLogger(LOGGER_NAME)

# pandoc does not support indexing. This will need to be engineered, or perhaps an extension exists.
# https://tex.stackexchange.com/questions/429707/making-index-in-pandoc-generated-ebook

# Inserting background images:
# https://stackoverflow.com/questions/25279614/inserting-images-in-markdown-garbles-latex-background-image
# https://tex.stackexchange.com/questions/167719/how-to-use-background-image-in-latex

# Exports

__all__ = (
    "BaseLoader",
    "Manifest",
    "Outline",
    "Topic",
    "Tree",
)

# Classes


class BaseLoader(File):
    """Base loader for implementing content loader classes.

    .. note::
        Loaders are assumed to operate from the ``source/`` directory in the project's root. The path given upon
        instantiation may be a directory or a file that is used to find files for export.

    """

    def __init__(self, config, path, project, context=None, output_format=OUTPUT_PRINTABLE):
        """Initialize a loader.

        :param config: The configuration instance for the project. If omitted the default config is used.
        :type config: ProjectConfig

        :param path: The path to loader content. See child classes for implementations.
        :type path: str

        :param project: The current project instance.
        :type project: Project

        :param context: Context may be used to pre-process content files as Jinja 2 templates.
        :type context: Context

        :param output_format: The intended output format may be used by the loader or underlying file instances to make
                              decisions about file collection and pre/post parsing.
        :type output_format: str

        """
        super().__init__(path)

        self.config = config
        self.context = context
        self.is_loaded = False
        self.output_format = output_format
        self.project = project
        self.root = os.path.dirname(path)
        self._content_path = os.path.join(self.root, "content")
        self._files = list()

    @property
    def count(self):
        """Get a count of the files included in the load.

        :rtype: int

        .. note::
            Count will be 0 if content has not yet been loaded. See ``load()``.

        .. tip::
            Files included within other content files do *not* increase the count.

        """
        return len(self._files)

    def __iter__(self):
        return iter(self._files)

    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__, self.path)

    def export_slideshow(self):
        """Export slideshow lines found in content files.

        :rtype: str

        """
        a = list()
        for f in self._files:
            b = f.get_slideshow()
            if len(b) > 0:
                a += f.get_slideshow()

        if len(a) == 0:
            return ""

        path = os.path.join(self.project.root, "temporary", "slidehow.markdown")
        content = "\n".join(a)
        write_file(path, content)

    def get_bibliography_file(self):
        """Get the the bibliography file.

        :rtype: BibliographyFile

        """
        return BibliographyFile(self.project)

    def get_comments(self):
        """Get the comments discovered during file processing.

        :rtype: list[Comment]

        """
        a = list()
        for f in self._files:
            a += f.comments

        return a

    def get_content_files(self):
        """Get a list of content files.

        :rtype: list[ContentFile]

        """
        raise NotImplementedError()

    def get_image_files(self, extensions=None):
        """Get a list of images included in the content.

        :param extensions: A list of extensions (including) the dot. If given as a string, the extensions should be
                           space separated.
        :type extensions: list | str

        :rtype: list[ImageFile]

        """
        if extensions is None:
            extensions = self.config.images.get("extensions", default=STATIC_EXTENSIONS)

        count = 0
        _files = list()
        for root, dirs, files in os.walk(self._content_path):
            for f in files:
                file_path = os.path.join(root, f)
                file = ImageFile(file_path)

                if file.extension not in extensions:
                    continue

                _files.append(file)

                count += 1

        return _files

    def get_replacement_text(self):
        """Get replacement text key/value pairs from the ``settings/replace.cfg`` file.

        :rtype: dict

        """
        path = os.path.join(self.root, "settings", "replace.cfg")
        if not os.path.exists(path):
            return None

        log.info("Loading replacement text: %s" % path)

        lines = read_file(path).split("\n")
        replace = dict()
        for line in lines:
            if len(line) == 0:
                continue

            if line.startswith("#") or line.startswith(";"):
                continue

            key, value = line.split("=")
            key = key.strip()
            value = value.strip()
            replace[key] = value

        return replace

    def get_template_files(self):
        """Get a list of template files used to generate the content.

        :rtype: list[TemplateFile]

        """
        # TODO: Make the templates directory name configurable.
        path = os.path.join(self.root, "templates")

        count = 0
        _files = list()
        for root, dirs, files in os.walk(path):
            for f in files:
                file_path = os.path.join(root, f)
                file = TemplateFile(file_path)

                _files.append(file)

                count += 1

        return _files

    def get_todos(self):
        """Get the to-dos discovered during file processing.

        :rtype: list[Todo]

        """
        a = list()
        for f in self._files:
            a += f.todos

        return a

    @property
    def has_bibliography_file(self):
        """Indicates whether a bibliography file exists.

        :rtype: bool

        """
        file = self.get_bibliography_file()
        return file.exists

    def load(self):
        """Load content files.

        :rtype: bool

        """
        if not self.exists:
            log.error("Loading path does not exist: %s" % self.path)
            return False

        self.get_content_files()
        # for f in self._files:
        #     registry.add(f)

        if len(self._files) > 0:
            self.is_loaded = True

        return self.is_loaded


class Manifest(BaseLoader):
    """Collect content file paths from a MANIFEST.txt file."""

    def get_content_files(self):
        """Load the manifest.

        :rtype: list[ContentFile]

        """
        # Read the contents of the manifest.
        content = read_file(self.path)

        # Count is used to identify missing files by line number.
        count = 0

        # Split into lines and iterate over the result.
        lines = content.split("\n")
        for relative_path in lines:
            if len(relative_path) == 0 or relative_path.startswith("#"):
                continue

            count += 1

            # Define the possible locations of the file.
            locations = [
                os.path.join(self.root, "content", relative_path + self.config.input.extension),
                os.path.join(self.root, "content", relative_path),
            ]

            # Look for the file in the locations.
            path = None
            for location in locations:
                if os.path.exists(location):
                    path = location
                    break

            # Zoinks! The file could not be found. Let the user know, but don't stop looking for files.
            if path is None:
                log.warning("Could not locate file named in MANIFEST.txt, line %s: %s" % (count, relative_path))
                continue

            # Initialize and save to the registry.
            self._files.append(ContentFile(path, self.project, context=self.context, output_format=self.output_format))

        self.is_loaded = True

        return self._files


class Outline(BaseLoader):
    """An OPML file."""

    def get_content_files(self):
        """Load the outline.

        :rtype: list[ContentFile]

        """
        # The loader's path is the path to the OPML file.
        outline = opml.parse(self.path)

        # Gather files identified in topics within the OPML file.
        source_files = list()
        for element in outline:
            topic = Topic(element, self.root)
            source_files += topic.get_files()

        # Create instances for each file identified.
        for f in source_files:
            self._files.append(ContentFile(f, self.project, context=self.context, output_format=self.output_format))

        self.is_loaded = True

        return self._files


class Topic(object):
    """A topic (element) within an OMPL file."""

    def __init__(self, element, path):
        self.title = element.text
        self._base_path = path

        self._children = list()
        if len(element) > 0:
            for item in element:
                self._children.append(item)
            self.has_children = True
        else:
            self.has_children = False

        try:
            self.name = element.file
        except AttributeError:
            self.name = str(slugify(self.title.lower()).replace("-", "_"))

        self.path = None
        file_name = self.name
        for ext in PAGE_EXTENSIONS:
            path = os.path.join(self._base_path, "content", "%s.%s" % (file_name, ext))
            if os.path.exists(path):
                self.path = path
                break

        if self.path is None:
            log.warning("Could not locate file named in outline.opml, %s: %s" % (self.title, file_name))

        try:
            skipped = element.skip
            if "checked" == skipped:
                self.skipped = True
            else:
                self.skipped = False
        except AttributeError:
            self.skipped = False

    def get_children(self):
        """Return a list of children as ``Topic`` instances. Note the list may
        be empty.

        :rtype: list

        """
        children = list()

        if not self.has_children:
            return children

        for item in self._children:
            children.append(Topic(item, self._base_path))

        return children

    def get_files(self):
        """Get the content files associated with the current topic."""
        return self._get_files(self)

    # noinspection PyMethodMayBeStatic
    def _get_files(self, topic):
        files = list()

        # print topic.name, topic.skipped

        if topic.skipped:
            return files

        if topic.path:
            files.append(topic.path)

        if topic.has_children:
            for child in topic.get_children():
                files += child.get_files()

        return files


class Tree(BaseLoader):
    """Load content files from the directory structure."""

    def get_content_files(self):
        """Load content files from the directory structure.

        :rtype: list[ContentFile]

        """
        for root, dirs, files in os.walk(self.path):
            files.sort()
            for f in files:
                file_path = os.path.join(root, f)
                file = ContentFile(file_path, self.project, context=self.context, output_format=self.output_format)

                if file.extension != self.config.input.extension:
                    continue

                self._files.append(file)

        self.is_loaded = True

        return self._files

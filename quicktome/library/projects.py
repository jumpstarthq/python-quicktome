# Imports

import logging
from myninjas import prompt
import os
from ..constants import INPUT_LOADER_CHOICES, INPUT_LOADER_TREE, MARKDOWN_EXTENSIONS, OUTPUT_PRINTABLE
from ..variables import AUTHOR_NAME, AUTHOR_TITLE, LOGGER_NAME
from .loaders import Manifest, Outline, Tree

log = logging.getLogger(LOGGER_NAME)

# Exports

__all__ = (
    "NewProjectForm",
    "Project",
)

# Classes


class NewProjectForm(prompt.Form):
    """Prompt user (on the command line) for info on a new project."""

    heading1 = prompt.Divider(label="Project")
    title = prompt.String("Title", required=True)
    description = prompt.String("Description")
    edition = prompt.Integer("Edition", default=1)

    heading2 = prompt.Divider(label="Author")
    author_name = prompt.String("Author Name", default=AUTHOR_NAME)
    author_title = prompt.String("Author Title", default=AUTHOR_TITLE)
    copyright_name = prompt.String("Copyright Name", default="$author_name")

    heading3 = prompt.Divider(label="Input Options")
    input_extension = prompt.String("Input Extension", choices=MARKDOWN_EXTENSIONS, default=".markdown")
    input_loader = prompt.String(
        "Input Loader",
        choices=INPUT_LOADER_CHOICES,
        default=INPUT_LOADER_TREE
    )


class Project(object):
    """A project is a collection of content, assets, and configuration that may be used to build a finished work."""

    def __init__(self, root):
        """Initialize the project.

        :param root: The path to the project.
        :type root: str

        """
        self.name = os.path.basename(root)
        self.root = root

    def create_path(self, *parts):
        """Create the given path if it does not already exist.

        The parts are joined to create the path relative to the project's root.

        :rtype: str
        :returns: The (full) joined path.

        """
        partial = os.path.join(*parts)
        full = os.path.join(self.root, partial)

        if not self.path_exists(partial):
            os.makedirs(full)

        return full

    def get_loader(self, config, context=None, output_format=OUTPUT_PRINTABLE):
        """Get the instance used to load content.

        :param config: The project's configuration instance.
        :type config: Config

        :param context: The context instance.
        :type context: Context

        :param output_format: The intended output format.
        :type output_format: str

        :rtype: BaseType[Content]

        """
        if self.has_manifest:
            log.info("Loading content files from MANIFEST.txt")
            path = self.get_path("source", "Manifest.txt")
            loader_class = Manifest
        elif self.has_opml:
            log.info("Loading content files from outline.opml")
            path = self.get_path("source", "outline.opml")
            loader_class = Outline
        else:
            log.info("Loading content files from directory structure.")
            path = self.get_path("source", "content")
            loader_class = Tree

        return loader_class(
            config,
            path,
            self,
            context=context,
            output_format=output_format
        )

    def get_path(self, *parts):
        """Get the full path to the given parts relative to that of the project's root.

        The parts are joined to test derive path.

        :rtype: str

        """
        return os.path.join(self.root, *parts)

    @property
    def has_manifest(self):
        """Indicates that a ``MANIFEST.txt`` file is present.

        :rtype: bool

        """
        return self.path_exists("source", "MANIFEST.txt")

    @property
    def has_opml(self):
        """Indicates that an ``outline.opml`` file is present.

        :rtype: bool

        """
        return self.path_exists("source", "outline.opml")

    def path_exists(self, *parts):
        """Indicates the given file or path exists relative to that of the project's root.

        The parts are joined to test the path.

        :rtype: bool

        """
        _path = os.path.join(self.root, *parts)
        return os.path.exists(_path)

# Imports

import logging
from myninjas.utils import File, copy_file, parse_jinja_string, parse_jinja_template, read_csv, read_file
from myninjas.tables import Table
import opml
import os
from .latex import BackgroundImage, Watermark
from ..constants import BIBLIOGRAPHY_EXTENSIONS, OUTPUT_EPUB, OUTPUT_HTML, OUTPUT_PDF, OUTPUT_PRINTABLE
from ..variables import LOGGER_NAME
from .registry import registry

log = logging.getLogger(LOGGER_NAME)

# Exports

__all__ = (
    "BibliographyFile",
    "Comment",
    "ContentFile",
    "DataFile",
    "ImageFile",
    "Process",
    "ProcessOutline",
    "TemplateFile",
    "Todo",
)


class Comment(object):
    """A placeholder class that captures ``.. comment::`` directives in content files"""

    def __init__(self, content, line_number=None, path=None):
        """Initialize the instance.

        :param content: The content, i.e., the comment.
        :type content: str

        :param line_number: The line number where the comment was found.
        :type line_number: int

        :param path: The path of the file where the comment was found.
        :type path: str

        """
        self.content = content
        self.line_number = line_number
        self.path = path


class Todo(object):
    """A placeholder class that captures ``.. todo::`` directives in content files."""

    def __init__(self, content, line_number=None, path=None):
        """Initialize the instance.

        :param content: The content, i.e., description of the task.
        :type content: str

        :param line_number: The line number where the task was found.
        :type line_number: int

        :param path: The path of the file where the task was found.
        :type path: str

        """
        self.content = content
        self.line_number = line_number
        self.path = path

# Classes


class BibliographyFile(File):
    """Represents one of Pandoc's support bibliography formats."""

    def __init__(self, project):
        """The file path is determined automatically."""
        path = ""
        for extension in BIBLIOGRAPHY_EXTENSIONS:
            name = "bibliography" + extension
            if project.path_exists("source", name):
                path = project.get_path("source", name)

        super().__init__(path)

    def copy(self, path):
        """Copy the file to the given path."""
        return copy_file(self.path, path)

    @property
    def exists(self):
        """Overridden to determine if a valid path was found."""
        return len(self.path) > 0


class ContentFile(File):
    """Content files are combined into a single input file used to generate pandoc output."""

    def __init__(self, path, project, context=None, output_format=OUTPUT_PRINTABLE):
        """Initialize a content file.

        :param path: The path to the file.
        :type path: str

        :param project: The current project instance.
        :type project: Project

        :param context: If given, the file will be parsed as a Jinja2 template.
        :type context: Context

        :param output_format: The intended output format.
        :type output_format: str

        """
        super().__init__(path)

        self.comments = list()
        self.content = ""
        self.context = context
        self.is_loaded = False
        self.output_format = output_format
        self.project = project
        self.root = os.path.dirname(path)
        self.todos = list()
        self._content_path = project.get_path("source", "content")
        self._data_path = project.get_path("source", "content", "_data")
        self._include_path = project.get_path("source", "content", "_includes")
        self._index_entries = dict()
        self._slideshow = list()

    def get_slideshow(self):
        """Get the slideshow lines, if any, that have been discovered in the content.

        :rtype: list[str]

        """
        return self._slideshow

    def load(self):
        """Load the content of the file, setting the ``content`` attribute of the instance.

        :rtype: bool
        :returns: ``True`` if the file was successfully loaded. The ``is_loaded`` is also set to ``True``.

        """
        # Issue a warning if the file does not exist.
        if not self.exists:
            log.warning("Content file does not exist: %s" % self.path)
            return False

        # Get the content of the file.
        content = read_file(self.path)

        # Pre-process content before parsing it as a template. This eliminates Jinja errors for text that conflicts with
        # template processing.
        content = self._preprocess(content)

        # Get the content of the file.
        # jinja2.exceptions.UndefinedError:
        if self.context is not None:
            content = parse_jinja_string(content, self.context.mapping())

        self.content = content

        self.is_loaded = True

        registry.mark_used(self)

        return True

    # noinspection PyMethodMayBeStatic
    def _get_annotations(self):
        """Get the recognized annotation directive names.

        :rtype: list[str]

        """
        return [
            "callout",
            "important",
            "info",
            "note",
            "tip",
            "warning",
        ]

    def _get_line_annotation(self, line):
        """Get the annotation, if any, that a given lines starts with.

        :param line: The line being parsed. See ``_preprocess()``.
        :type line: str

        :rtype: str | None

        """
        annotations = self._get_annotations()
        for a in annotations:
            if line.startswith(".. %s::" % a):
                return a

            if line.startswith(".. %s-title::" % a):
                return "%s-title" % a

        return None

    def _load_include(self, file_name, line_number):
        """Load a file included by the current file.

        :param file_name: The name (or relative path) of the included file.
        :type file_name: str

        :rtype: str

        """
        # Define the possible locations for the include file, including automatically adding the input extension.
        locations = [
            os.path.join(self.root, file_name + self.extension),
            os.path.join(self.root, "_includes", file_name + self.extension),
            os.path.join(self.root, file_name),
            os.path.join(self.root, "_includes", file_name),
            os.path.join(self._include_path, file_name + self.extension),
            os.path.join(self._include_path, file_name),
        ]

        # Attempt to locate the file.
        path = None
        for location in locations:
            if os.path.exists(location):
                path = location
                break

        if path is None:
            message = "Could not find include file referenced by %s (line %s): %s"
            log.warning(message % (self.basename, line_number, file_name))
            return ""

        file = ContentFile(path, self.project, context=self.context)
        file.load()

        self._slideshow += file._slideshow

        return file.content

    def _load_table(self, file_name, line_number):
        """Load a data file referenced in the current file.

        :param file_name: The name (or relative path) of the included file.
        :type file_name: str

        :rtype: str

        """
        # Define the possible locations of the file.
        locations = [
            os.path.join(self.root, file_name + ".csv"),
            os.path.join(self.root, file_name),
            os.path.join(self.root, "_data", file_name + ".csv"),
            os.path.join(self.root, "_data", file_name),
            os.path.join(self._data_path, file_name + ".csv"),
            os.path.join(self._data_path, file_name),
        ]

        # Attempt to locate the file.
        path = None
        for location in locations:
            if os.path.exists(location):
                path = location
                break

        if path is None:
            message = "Could not find data file referenced by %s (line %s): %s"
            log.warning(message % (self.basename, line_number, file_name))
            return ""

        # Load the data file.
        log.info("Loading data file: %s" % path)
        table = DataFile(path)
        table.load()

        registry.mark_used(table)

        return table.to_markdown()

    def _preprocess(self, content):
        """Parse the file for special directives that must be handled before content files are combined.

        :param content: The file content to be processed.
        :type content: str

        :rtype: str

        """
        # Parse content for the link replacement pattern when Markdown is in use.
        # if self.config.input.format == INPUT_FORMAT_MD:
        #     pattern = re.compile(r"\|[a-zA-Z]*\|")
        #     for match in pattern.findall(content):
        #         anchor = match.replace("|", "")
        #         replace = "[%s](#%s)" % (anchor, anchor)
        #         content = content.replace(content, replace)

        # Parse content for special directives.
        count = 1
        lines = content.split("\n")
        annotation_marker = None
        code_fence_marker = False
        slideshow_marker = False
        table = None
        _lines = list()
        for line in lines:

            if line.startswith("```") and len(line) > 3:
                code_fence_marker = True
            elif line.startswith("```"):
                code_fence_marker = False
            else:
                pass

            if not code_fence_marker:
                line = line.strip()

            if annotation_marker and len(line) == 0:
                if self.output_format in (OUTPUT_PDF, OUTPUT_PRINTABLE):
                    _lines.append(r"\end{%s}" % annotation_marker)
                    _lines.append("")
                elif self.output_format in (OUTPUT_EPUB, OUTPUT_HTML):
                    _lines.append("</blockquote>")
                    _lines.append("")
                else:
                    pass

                annotation_marker = None

            if slideshow_marker and len(line) == 0:
                self._slideshow.append("")
                slideshow_marker = False
            elif slideshow_marker and len(line) > 0:
                self._slideshow.append(line)
            else:
                pass

            if table is not None:
                if len(line) == 0:
                    _lines.append(table.to_string())
                    table = None
                else:
                    if table.headings is None:
                        table.headings = list()
                        for i in line.split(","):
                            table.headings.append(i.strip())
                    else:
                        table.add(line.split(","))

                    continue

            annotation = self._get_line_annotation(line)
            if annotation is not None:
                tokens = line.split("::")

                annotation_title = None
                if len(tokens[1]) > 0:
                    annotation_title = tokens[1].strip()

                if self.output_format in (OUTPUT_PDF, OUTPUT_PRINTABLE):
                    if annotation_title is not None:
                        annotation += "-title"
                        directive = r"\begin{%s}{%s}" % (annotation, annotation_title)
                    else:
                        directive = r"\begin{%s}" % annotation

                    _lines.append(directive)
                elif self.output_format in (OUTPUT_EPUB, OUTPUT_HTML):
                    _lines.append('<blockquote class="annotation %s">' % annotation)
                    if annotation_title is not None:
                        _lines.append('<h3 class="annotation-title %s-title">%s</h3>' % (annotation, annotation_title))
                else:
                    pass

                annotation_marker = annotation
            elif line.startswith(".. a::"):
                # see https://tex.stackexchange.com/questions/139106/referencing-tables-in-pandoc
                anchor = line.split("::")[-1].strip()
                # _lines.append("\phantomsection\label{%s}" % anchor)
                # _lines.append("")

                if self.output_format in (OUTPUT_PDF, OUTPUT_PRINTABLE):
                    _lines.append(r"\hypertarget{%s}{}" % anchor)
                    _lines.append("")
                elif self.output_format in (OUTPUT_EPUB, OUTPUT_HTML):
                    _lines.append('<a name="%s">' % anchor)
                    _lines.append("")
                else:
                    pass
            elif line.startswith(".. background::"):
                tokens = line.split("::")[-1].split(",")
                path = tokens.pop(0).strip()

                kwargs = dict()
                for t in tokens:
                    key, value = t.split("=")
                    kwargs[key.strip()] = value.strip()

                image = BackgroundImage(path, **kwargs)
                _lines.append(image.to_tex())
                _lines.append("")
            elif line.startswith(".. blank::"):
                _lines.append(r"\afterpage{\blankpage}")
                _lines.append("")
            elif line.startswith(".. break::"):
                _lines.append(r"\newpage")
            elif line.startswith(".. columns::"):
                number = line.split(".. columns::")[-1]
                _lines.append(r"\Begin{multicols}{%s}" % number)
            elif line.startswith(".. columns-end::"):
                _lines.append(r"\End{multicols}")
            elif line.startswith(".. comment::"):
                content = line.split("::")[1].strip()
                self.comments.append(Comment(content, path=self.path, line_number=count))
            elif line.startswith(".. drop::"):
                word = line.split(".. drop::")[1].split(" ")[0]
                letter = word[0]
                remaining_word = word[1:]
                remaining_line = line[len(word) + 9:]

                if self.output_format in (OUTPUT_PDF, OUTPUT_PRINTABLE):
                    _lines.append(r"\lettrine[lines=4,slope=-4pt,nindent=8pt]{%s}%s %s" % (
                        letter,
                        remaining_word,
                        remaining_line
                    ))
                elif self.output_format in (OUTPUT_EPUB, OUTPUT_HTML):
                    _lines.append('<span class="dropcap">%s</span> %s %s' % (
                        letter,
                        remaining_word,
                        remaining_line
                    ))
                else:
                    _lines.append(line)
            elif line.startswith(".. include::"):
                file_name = line.split("::")[-1].strip()
                _lines.append(self._load_include(file_name, count))
            elif line.startswith(".. index::"):
                term = line.split("::")[-1].strip()
                if self.output_format in (OUTPUT_PDF, OUTPUT_PRINTABLE):
                    _lines.append(r"\index{%s}" % term)
            elif line.startswith(".. slideshow::"):
                slideshow_marker = True
                _lines.append("")
            elif line.startswith(".. space::"):
                _lines.append(r"\ \ ")
            elif line.startswith(".. standout::"):
                content = line.split(".. standout::")[1].strip()

                if self.output_format in (OUTPUT_PDF, OUTPUT_PRINTABLE):
                    _lines.append(r"\begin{center}  ")
                    _lines.append(r"\textbf{\Large{%s}}" % content)
                    _lines.append(r"\end{center}")
                    _lines.append("")
                elif self.output_format in (OUTPUT_EPUB, OUTPUT_HTML):
                    _lines.append('<p class="standout">%s</p>' % content)
                    _lines.append("")
                else:
                    _lines.append(line)
            elif line.startswith(".. style::"):
                style_name = line.split(".. style::")[1].strip()

                if self.output_format in (OUTPUT_PDF, OUTPUT_PRINTABLE):
                    _lines.append(r"\begin{%s}" % style_name)
                elif self.output_format in (OUTPUT_EPUB, OUTPUT_HTML):
                    _lines.append('<div class="%s">' % style_name)
                    _lines.append("")
                else:
                    pass
            elif line.startswith(".. style-end::"):
                style_name = line.split(".. style-end::")[1].strip()
                if self.output_format in (OUTPUT_PDF, OUTPUT_PRINTABLE):
                    _lines.append(r"\end{%s}" % style_name)
                elif self.output_format in (OUTPUT_EPUB, OUTPUT_HTML):
                    _lines.append('</div>')
                    _lines.append("")
                else:
                    pass
            elif line.startswith(".. table::"):
                file_name = line.split(".. table::")[-1].strip()
                if file_name:
                    _lines.append(self._load_table(file_name, count))
                else:
                    table = Table(formatting="simple")
            elif line.startswith(".. todo::"):
                content = line.split("::")[1].strip()
                self.todos.append(Todo(content, path=self.path, line_number=count))
            elif line.startswith(".. watermark::"):
                tokens = line.split("::")[-1].split(",")
                label = tokens.pop(0).strip()

                kwargs = dict()
                for t in tokens:
                    key, value = t.split("=")
                    kwargs[key.strip()] = value.strip()

                image = Watermark(label, **kwargs)
                _lines.append(image.to_tex())
                _lines.append("")
            else:
                _lines.append(line)

            count += 1

        return "\n".join(_lines)


class DataFile(File):
    """A data file (CSV) referenced within content files.

    .. note::
        This is only used when processing the ``.. table::`` shortcut in Markdown files. reStructuredText has its own
        ``.. table::`` directive.

    """

    def __init__(self, path):
        """Initialize the data file."""
        super().__init__(path)

        self.headings = list()
        self.is_loaded = False
        self.rows = list()

    def load(self):
        """Load the table.

        :rtype: bool

        """
        if not self.exists:
            return False

        data = read_csv(self.path)
        self.headings = data.pop(0)
        self.rows = data

        self.is_loaded = True

        return True

    def to_markdown(self):
        """Export the table as Markdown.

        :rtype: str

        .. note::
            According to the python-tabluate documentation, the ``simple`` format corresponds to the ``simple_tables``
            extension for Pandoc's Markdown.

            See http://johnmacfarlane.net/pandoc/README.html#tables

        """
        table = Table(headings=self.headings, formatting="simple")

        for row in self.rows:
            table.add(row)

        return table.to_string()

        # count = 0
        # lines = list()
        # for row in self.rows:
        #     lines.append("|".join(row))
        #
        #     if count == 0:
        #         headers = list()
        #         for header in row:
        #             headers.append(len(header) * "-")
        #
        #         lines.append("|".join(headers))
        #
        #     count += 1
        #
        # lines.append("")
        #
        # return "\n".join(lines)

    def to_rst(self):
        """Export the table as reStructuredText.

        :rtype: str

        .. note::
            This method is probably not used since reStructuredText includes it's own ``.. table::`` directive.
            See http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html#tables

        """
        table = Table(headings=self.headings, formatting="rst")

        for row in self.rows:
            table.add(row)

        return table.to_string()


class ImageFile(File):
    """An image file to be included as content."""

    def copy(self, path):
        """Copy the file to the given path.

        :param path: The path to which the file should be copied, including the file name.
        :type path: str

        """
        return copy_file(self.path, path, make_directories=True)


class TemplateFile(File):
    """Templates are used in pandoc output."""

    def copy(self, path):
        """Copy the file to the given path.

        :param path: The path to which the file should be copied, including the file name.
        :type path: str

        :rtype: list[bool, error]

        """
        return copy_file(self.path, path, make_directories=True)


# Process Parsing


class Process(object):
    """Load process documentation from an OPML file."""

    def __init__(self, element, project, level=1):
        """Initialize a process.

        :param element: The OPML element.

        :param project: The current project instance.
        :type project: Project

        :param level: The level of the element within the outline.
        :type level: int

        """
        self.description = None
        self.inputs = list()
        self.level = level
        self.name = element.text.lower().replace(" ", "-")
        self.objective = None
        self.outputs = list()
        self.project = project
        self.raci = {
            'accountable': None,
            'consulted': list(),
            'informed': list(),
            'responsible': list(),
        }
        self.timescale = None
        self.title = element.text
        self.trigger = None

        self.diagram = os.path.join("_images", self.name + ".png")

        # The opml module does not populate properties that are empty, so we must check for each one to avoid an
        # AttributeError. These standard elements may be handled thusly.
        attrs = (
            "description",
            "objective",
            "timescale",
            "trigger",
        )
        for n in attrs:
            value = getattr(element, n, None)
            setattr(self, n, value)

        # Inputs and outputs may be comma separated.
        inputs = getattr(element, "inputs", None)
        if inputs is not None:
            inputs = inputs.split(",")
            for i in inputs:
                self.inputs.append(i.strip())

        outputs = getattr(element, "outputs", None)
        if outputs is not None:
            outputs = outputs.split(",")
            for o in outputs:
                self.outputs.append(o.strip())

        # Identify processes that mark a "gate" in decision making.
        try:
            gate = element.gate
            if "checked" == gate:
                self.is_gate = True
            else:
                self.is_gate = False
        except AttributeError:
            self.is_gate = False

        # It is useful to disable some topics from appearing in the final output.
        try:
            skipped = element.skipped
            if "checked" == skipped:
                self.skipped = True
            else:
                self.skipped = False
        except AttributeError:
            self.skipped = False

        # Get child elements.
        self._children = list()
        if len(element) > 0:
            self.has_children = True
            for item in element:
                self._children.append(Process(item, self.project, level=level + 1))
        else:
            self.has_children = False

    def get_children(self):
        """Get sub-processes.

        :rtype: list[Process]

        """
        return self._children

    def to_markdown(self):
        """Export the process as Markdown.

        :rtype: str

        """
        context = {
            'process': self,
        }
        if self.level == 1:
            context['page_break'] = True

        template = os.path.join(self.project.root, "templates", "process.j2.markdown")
        return parse_jinja_template(template, context)


class ProcessOutline(File):
    """Parses pre-defined OPML for process definitions."""

    def __init__(self, path, project):
        """Initialize the outline.

        :param path: The path the outline file.
        :type path: str

        :param project: The current project instance.
        :type project: Project

        """
        super().__init__(path)

        self.is_loaded = False
        self.processes = list()
        self.project = project

    def __iter__(self):
        return iter(self.processes)

    @classmethod
    def nesting_machine(cls, start, items=None):
        """Assists with recursively finding sub-processes.

        :param start: The starting point of the recursion.
        :type start: list[Process]

        :param items: Items that have already been found.
        :type items: list[Process]

        :rtype: list[Process]

        """
        if items is None:
            items = list()

        for i in start:
            items.append(i)
            if i.has_children:
                items = cls.nesting_machine(i.get_children(), items=items)

        return items

    def get_all_processes(self):
        """Get all processes, sub-processes, sub-sub-processes as a single list.

        :rtype: list[Process]

        """
        return self.nesting_machine(self.processes)

    def load(self):
        """Load the outline.

        :rtype: bool

        """
        if not self.exists:
            return False

        outline = opml.parse(self.path)
        for element in outline:
            self.processes.append(Process(element, self.project))

        self.is_loaded = True
        return True

# Imports

from datetime import datetime
import logging
from myninjas.settings import INIConfig
from ..constants import STATIC_EXTENSIONS
from ..variables import LOGGER_NAME

log = logging.getLogger(LOGGER_NAME)

# Exports

__all__ = (
    "ExtraVariablesConfig",
    "ProjectConfig",
    # "Section",
)

# Classes


class ExtraVariablesConfig(INIConfig):
    """Configuration class for loading extra variables."""

    def _get_default_sections(self):
        return list()

    def _get_default_values(self):
        return list()


class ProjectConfig(INIConfig):
    """The global project configuration from the ``meta.ini`` file if it exists."""

    def _get_default_sections(self):
        """Return the sections that are required for minimal operation."""
        return [
            "author",
            "copyright",
            "images",
            "input",
            "project",
        ]

    def _get_default_values(self):
        """Return section values required for minimal operation."""
        # section, key, default
        return [
            ("author", "name", "Anonymous"),
            ("copyright", "name", "Unknown"),
            ("copyright", "year", datetime.now().year),
            ("images", "directory", "_images"),
            ("images", "extensions", " ".join(STATIC_EXTENSIONS)),
            ("input", "directory", "temporary"),
            ("input", "extension", ".markdown"),
            ("input", "format", "markdown"),
            ("input", "loader", "tree"),
            ("input", "name", "Untitled-Project"),
            ("project", "description", None),
            ("project", "edition", 1),
            ("project", "root", "???"),
            ("project", "title", "Untitled Project"),
        ]

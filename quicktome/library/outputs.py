# Imports

from collections import OrderedDict
import logging
from myninjas.shell import Command
from myninjas.utils import copy_tree, read_file, write_file
import os
# noinspection PyPackageRequirements
from slugify import slugify
from ..constants import BIBLIOGRAPHY_EXTENSIONS, OUTPUT_DOCX, OUTPUT_EPUB, OUTPUT_HTML, OUTPUT_MOBI, OUTPUT_PDF, \
    OUTPUT_PRINTABLE, OUTPUT_SLIDESHOW
from ..variables import LOGGER_NAME, QUICKTOME_PATH

log = logging.getLogger(LOGGER_NAME)

# Exports

__all__ = (
    "MAPPING",
    "BaseOutput",
    "Docx",
    "EPub",
    "HTML",
    "Mobi",
    "PDF",
    "Printable",
    "Slideshow",
)

# Classes


class BaseOutput(object):
    """Base class for implementing output classes."""

    def __init__(self, config, project, staging, directory="../output", **kwargs):
        self.config = config
        self.directory = directory
        self.options = kwargs
        self.project = project
        self.root = os.path.join(self.project.root, directory)
        self.staging = staging

        if config.has("output", "name"):
            self.name = config.output.name
        else:
            self.name = slugify(config.project.title)

    # def __init__(self, name, root, input_directory="temporary", input_extension=".markdown", input_format="markdown",
    # output_directory="../output"):
    #     """Initialize the output.
    #
    #     :param name: The file name (without the extension) of the output file.
    #     :type name: str
    #
    #     :param root: The path to the project's root directory.
    #     :type root: str
    #
    #     :param input_directory: The name of the directory where input files are created or copied.
    #     :type input_directory: str
    #
    #     :param input_format: The format of the file used to generate final output.
    #     :type input_format: str
    #
    #     :param output_directory: The name or path of the directory where the output file is generated.
    #     :type output_directory: str
    #
    #     """
    #     self.input_directory = input_directory
    #     self.input_extension = input_extension
    #     self.input_format = input_format
    #     self.name = name
    #     self.output_directory = output_directory
    #     self.root = root

    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__, self.name)

    def get_bibliography_file(self):
        """Get the bibliography file path (if it exists).

        :rtype: str | None

        """
        for extension in BIBLIOGRAPHY_EXTENSIONS:
            path = os.path.join(self.root, "bibliography" + extension)
            if os.path.exists(path):
                return path

        return None

    def get_command(self):
        """Get the pandoc command used to generate the output.

        :rtype: Command

        """
        tokens = self.get_command_tokens()
        return Command(" ".join(tokens), path=self.staging.root)

    def get_command_tokens(self):
        """Get the tokens used for executing the command.

        :rtype: list[str]

        """
        raise NotImplementedError()

    @classmethod
    def get_extension(cls):
        """Get the extension of the output file.

        :rtype: str

        """
        return cls.__name__.lower()

    def get_input_file(self):
        """Get the name of the input file.

        :rtype: str

        """
        return "%s%s" % (self.name, self.config.input.extension)

    @classmethod
    def get_meta_values(cls):
        """Get the default ``meta.ini`` that may be used to initialize the output instance.

        :rtype: OrderedDict

        """
        raise NotImplementedError()

    def get_output_path(self):
        """Get the output path.

        :rtype: str

        """
        return os.path.join(self.directory, "%s.%s" % (self.name, self.get_extension()))

    def prepare(self):
        """Prepare the output or stating directories as needed.

        .. note::
            This method does nothing by default. Child classes may perform their own preparation steps.

        :rtype: bool

        """
        return True

    def write(self):
        """Convert input files to the output format.

        :rtype: bool

        .. note::
            By default, this simply runs the pandoc command. Child classes may override to perform additional steps
            before or after the conversion.

        """
        command = self.get_command()
        if command.run():
            return True

        log.warning("Conversion appears to have failed:")
        log.warning(command.error)

        return False


class Docx(BaseOutput):
    """Export content as an MS Word document."""

    def __init__(self, config, project, staging, template="templates/docx/default.docx", **kwargs):
        """Initialize the output.

        :param template: The Word reference template.
        :type template: str

        .. note::
            Args and kwargs are passed to the underlying :py:class:`BaseOutput` class.

        """
        super().__init__(config, project, staging, **kwargs)

        self.template = template

    def get_command_tokens(self):
        a = list()
        a.append("pandoc -f markdown")
        a.append("-o %s" % self.get_output_path())
        a.append("-t docx")

        if self.template:
            a.append("--reference-docx=%s" % self.template)

        a.append(self.get_input_file())

        return a

    @classmethod
    def get_meta_values(cls):
        """Get docx output options."""
        d = OrderedDict()
        d['template'] = "templates/docx/default.docx"

        return d


class EPub(BaseOutput):
    """Export content as an epub file."""

    def __init__(self, config, project, staging, cover="templates/epub/cover.jpg",
                 stylesheet="templates/epub/style.css", toc=True, **kwargs):
        """Initialize the output.

        :param cover: The cover image to use.
        :type cover: str

        :param stylesheet: The epub stylesheet.
        :type stylesheet: str

        :param toc: Indicates whether a table of contents should be included.
        :type toc: bool

        .. note::
            Args and kwargs are passed to the underlying :py:class:`BaseOutput` class.

        """
        super().__init__(config, project, staging, **kwargs)

        self.cover = cover
        self.include_toc = toc
        self.stylesheet = stylesheet

    def get_command_tokens(self):
        a = list()
        a.append("pandoc -f markdown")
        a.append("--base-header-level=1")
        a.append("-o %s" % self.get_output_path())
        a.append("--self-contained")
        a.append("--epub-cover-image=%s" % self.cover)
        a.append("--epub-stylesheet=%s" % self.stylesheet)
        a.append(self.get_input_file())

        return a

    @classmethod
    def get_meta_values(cls):
        """Get epub output options."""
        d = OrderedDict()
        d['cover'] = "templates/epub/cover.jpg"
        d['stylesheet'] = "templates/epub/style.css"
        d['toc'] = True

        return d


class HTML(BaseOutput):
    """Export content as a standalone HTML file."""

    def __init__(self, config, project, staging, footer="templates/html/footer.html",
                 header="templates/html/header.html", toc=True, **kwargs):
        """Initialize the output.

        :param footer: The footer file to use.
        :type footer: str

        :param header: The header file to use.
        :type header: str

        :param toc: Indicates whether a table of contents should be included.
        :type toc: bool

        .. note::
            Args and kwargs are passed to the underlying :py:class:`BaseOutput` class.

        """
        super().__init__(config, project, staging, **kwargs)

        self.footer = footer
        self.header = header
        self.include_toc = toc

    def get_command_tokens(self):
        a = list()
        a.append("pandoc -f markdown")

        if self.footer:
            a.append("-A %s" % self.footer)

        if self.header:
            a.append("--include-in-header=%s" % self.header)

        a.append("-o %s" % self.get_output_path())
        a.append("--standalone")

        if self.include_toc:
            a.append("--toc")

        a.append(self.get_input_file())

        return a

    @classmethod
    def get_meta_values(cls):
        """Get HTML output options."""
        d = OrderedDict()
        d['footer'] = "templates/html/footer.html"
        d['header'] = "templates/html/header.html"
        d['toc'] = True

        return d


class Mobi(BaseOutput):
    """Export content as a mobi file.

    .. note::
        The content must first be exported in epub format. Additionally, the kindlegen command must also be installed.

    """

    def get_command_tokens(self):
        a = list()
        a.append("kindlegen -verbose")

        a.append("-o %s" % self.get_output_path())

        a.append(os.path.join(self.root, "%s.epub" % self.name))

        return a

    @classmethod
    def get_meta_values(cls):
        """Get mobi output options."""
        return OrderedDict()


class PDF(BaseOutput):
    """Export content as a PDF file."""

    def __init__(self, config, project, staging, cover="templates/pdf/cover.pdf", font_family="bookman", font_size=24,
                 index=False, lof=True, lot=True, template="templates/pdf/default.tex", toc=True, **kwargs):
        """Initialize the output.

        :param cover: A PDF whose first page may be used as the cover of the output file. This is optional, and if the
                      specified file does not exist, the cover will be omitted.
        :type cover: str

        :param font_family: The font family to use.
        :type font_family: str

        :param font_size: The size of the font.
        :type font_size: int

        :param template: The LaTex template to use.
        :type template: str

        :param toc: Indicates whether a table of contents should be included.
        :type toc: bool

        .. note::
            Args and kwargs are passed to the underlying :py:class:`BaseOutput` class.

        """
        super().__init__(config, project, staging, **kwargs)

        self.cover = cover
        self.include_index = index
        self.include_lof = lof
        self.include_lot = lot
        self.include_toc = toc
        self.font_family = font_family
        self.font_size = font_size
        self.template = template

    def get_command_tokens(self):

        a = list()

        a.append("pandoc -f markdown")

        # a.append("-A templates/pdf/after_body.tex")
        path = self.get_bibliography_file()
        if path is not None:
            a.append("--filter pandoc-citeproc --bibliography=%s" % path)

        a.append("-o %s" % self.get_output_path())

        if self.include_lof:
            a.append("-V lof")

        if self.include_lof:
            a.append("-V lot")

        # xelatex appears to be required when unicode characters exist in the source file. This might also be handled
        # in the tex template, but using the switch here ensures that there's a fighting chance of dealing with encoding
        # errors.
        a.append("--pdf-engine=xelatex")

        a.append("-t latex")

        if self.include_toc:
            a.append("--toc")

        if self.template:
            a.append("--template=%s" % self.template)

        # This was producing an error with pandoc 1.9.x but appears to be working as of 2.5.
        a.append("-V colorlinks=true")

        if self.cover is not None:
            a.append("-V cover=%s" % self.cover)

        if self.font_family:
            a.append("-V fontfamily=%s" % self.font_family)

        if self.font_size:
            a.append("-V fontsize=%spt" % self.font_size)

        if self.options:
            for key, value in self.options.items():
                key = key.replace("_", "-")
                if value is True:
                    a.append("--%s" % key)
                else:
                    a.append('--%s="%s"' % (key, value))

        a.append(self.get_input_file())

        return a

    @classmethod
    def get_meta_values(cls):
        """Get PDF output options."""
        d = OrderedDict()
        d['cover'] = "pdf/cover.pdf"
        d['font_family'] = "bookman"
        d['font_size'] = 24
        d['template'] = "templates/pdf/default.tex"
        d['toc'] = True

        return d

    def write(self):
        """Additional processing occurs when ``index=True``."""
        if self.include_index:
            self._generate_index()

        return super().write()

    def _generate_index(self):
        """Generate an index."""

        # First process Markdown to latex without outputing the PDF.
        tokens = self.get_command_tokens()
        tokens[1] = "-o %s.tex" % self.name

        command = Command(" ".join(tokens), path=self.staging.root)
        command.run()

        # Manually force the creation of the index file for parsing below.
        command = Command("pdflatex -draftmode %s.tex" % self.name, path=self.staging.root)
        command.run()

        # Parse the contents of the index file to create links to assemble an index.
        path = os.path.join(self.staging.root, "%s.idx" % self.name)
        contents = read_file(path).split("\n")
        index = dict()
        for line in contents:
            if len(line) == 0:
                continue

            tokens = line.split("|")

            term = tokens[0].split("{")[1].strip()
            page = tokens[1].split("hyperpage}")[1].replace("{", "").replace("}", "")

            if term not in index:
                index[term] = list()

            index[term].append(page)

        # print(index)

        # Find the unique letters in the index.
        letters = list()
        for term in index.keys():
            letter = term[0].upper()
            if letter not in letters:
                letters.append(letter)

        # Create a heading and links for each term that was found above.
        a = list()
        letters.sort()
        for letter in letters:
            a.append("## %s" % letter)
            a.append("")

            for term, pages in index.items():
                if letter.lower() == term[0].lower():
                    b = list()
                    for page in pages:
                        b.append(r"\hyperlink{page.%s}{%s}" % (page, page))

                    a.append("%s ... %s" % (term, ", ".join(b)))
                    a.append("")

        # Add the terms to the end of the input file.
        path = os.path.join(self.staging.root, self.get_input_file())
        content = read_file(path)
        content += "\n".join(a)
        write_file(path, content)


class Printable(BaseOutput):
    """Export content as a print-ready PDF."""

    def __init__(self, config, project, staging, cover=None, font_family="bookman", font_size=14, lof=True, lot=True,
                 template="templates/printable/default.tex", toc=True, **kwargs):
        """Initialize the output.

        :param cover: A PDF whose first page may be used as the cover of the output file. This is optional, and if the
                      specified file does not exist, the cover will be omitted.
        :type cover: str

        :param font_family: The font family to use.
        :type font_family: str

        :param font_size: The size of the font.
        :type font_size: int

        :param template: The LaTex template to use.
        :type template: str

        :param toc: Indicates whether a table of contents should be included.
        :type toc: bool

        .. note::
            Args and kwargs are passed to the underlying :py:class:`BaseOutput` class.

        """
        super().__init__(config, project, staging, **kwargs)

        self.cover = cover
        self.include_lof = lof
        self.include_lot = lot
        self.include_toc = toc
        self.font_family = font_family
        self.font_size = font_size
        self.template = template

    def get_command_tokens(self):
        a = list()
        a.append("pandoc -f markdown")

        path = self.get_bibliography_file()
        if path is not None:
            a.append("--filter pandoc-citeproc --bibliography=%s" % path)

        a.append("-o %s" % self.get_output_path())

        if self.include_lof:
            a.append("-V lof")

        if self.include_lof:
            a.append("-V lot")

        a.append("-t latex")

        if self.cover is not None:
            path = os.path.join(self.root, "temporary", "templates", self.cover)
            if os.path.exists(path):
                a.append("-V cover=%s" % path)

        if self.include_toc:
            a.append("--toc")

        if self.template:
            a.append("--template=%s" % self.template)

        if self.font_family:
            a.append("-V fontfamily=%s" % self.font_family)

        if self.font_size:
            a.append("-V fontsize=%spt" % self.font_size)

        a.append("-V geometry:margin=2in")
        a.append("-V links-as-notes")

        a.append(self.get_input_file())

        return a

    @classmethod
    def get_extension(cls):
        return "pdf"

    @classmethod
    def get_meta_values(cls):
        """Get printable PDF output options."""
        d = OrderedDict()
        d['cover'] = None
        d['font_family'] = "bookman"
        d['font_size'] = 14
        d['template'] = "templates/printable/default.tex"
        d['toc'] = True

        return d

    def get_output_path(self):
        file_name = "%s-Printable.%s" % (self.name, self.get_extension())
        return os.path.join(self.directory, file_name)


class Slideshow(BaseOutput):
    """Export the content as a slideshow.

    .. warning::
        More work needs to be done for slideshow output.

    """
    # REVEALJS_THEME := league
    # REVEALJS_OPTS := -t revealjs -o index.html --standalone --css=revealjs/css/theme/$(REVEALJS_THEME).css
    # --slide-level 2 slideshow.txt;
    #
    # # S5
    # # Options for building a slideshow using the S5 library. See README for
    # # more information.
    # S5_OPTS := -t s5 -o index.html --standalone slideshow.txt;
    # SLIDESHOW_FORMAT := revealjs
    # SLIDESHOW_DIR := $(INPUT_DIR)/slideshow;

    def __init__(self, config, project, staging, level=3, slideshow_format="s5", theme=None, **kwargs):
        super().__init__(config, project, staging, **kwargs)

        self.level = level
        self.format = slideshow_format
        self.theme = theme

    def get_command(self):
        """The default for pandoc is to operate from the staging root, but slideshows must be generated from within the
        slideshow directory.
        """
        tokens = self.get_command_tokens()
        return Command(" ".join(tokens), path=os.path.join(self.staging.root, "%s-Slideshow" % self.name))

    def get_command_tokens(self):
        a = list()

        # path = os.path.join(self.root, "temporary", "slideshow")
        if self.format == "reveal":
            a.append("pandoc -f markdown")
            a.append("--css=revealjs/css/theme/%s" % self.theme or "league")
            a.append("-o index.html")
            a.append("--slide-level %s" % self.level)
            a.append("--standalone")
            a.append("-t revealjs")
            a.append("%s" % self.get_input_file())
        else:
            a.append("pandoc -f markdown")
            a.append("-o index.html")
            a.append("--standalone")
            a.append("-t s5")
            a.append("%s" % self.get_input_file())

        return a

    @classmethod
    def get_extension(cls):
        return "html"

    def get_input_file(self):
        return "../%s-Slideshow.markdown" % self.name

    @classmethod
    def get_meta_values(cls):
        """Get slideshow output options."""
        d = OrderedDict()
        d['format'] = "reveal"
        d['level'] = 2
        d['theme'] = "league"

        return d

    def get_output_path(self):
        return os.path.join("../", self.directory, "%s-Slideshow/index.html" % self.name)

    def prepare(self):
        path = os.path.join(self.staging.root, "%s-Slideshow" % self.name)
        if not os.path.exists(path):
            log.info("Preparing slideshow directory: %s" % path)
            os.makedirs(path)

        if self.format == "reveal":
            from_path = os.path.join(QUICKTOME_PATH, "bundled", "revealjs")
            to_path = os.path.join(path, "revealjs")
        else:
            from_path = os.path.join(QUICKTOME_PATH, "bundled", "s5")
            to_path = os.path.join(path, "s5")

        if not os.path.exists(to_path):
            log.info("Copying %s to slideshow directory." % to_path)
            os.makedirs(to_path)

        copy_tree(from_path, to_path)

        return True


MAPPING = {
    OUTPUT_DOCX: Docx,
    OUTPUT_EPUB: EPub,
    OUTPUT_HTML: HTML,
    OUTPUT_MOBI: Mobi,
    OUTPUT_PDF: PDF,
    OUTPUT_PRINTABLE: Printable,
    OUTPUT_SLIDESHOW: Slideshow
}

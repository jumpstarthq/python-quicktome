{% if page_break %}.. break::{% endif %}

{{ "#" * level }} {{ process.title }}

{% if process.description %}
{{ process.description }}

{% endif %}

{% if process.objective %}
Objective:  {{ process.objective }}

{% endif %}

{% if process.has_children %}
{{ "#" * (level + 1) }} Activities

{% for child in process.get_children() -%}
- [{{ child.title }}](#{{ child.slug }}){% if child.description %}: {{ child.description }}{% endif %}
{% endfor %}

{% endif %}

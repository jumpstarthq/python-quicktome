#! /usr/bin/env python

import sys
sys.path.insert(0, "/Users/shawn/Work/python-quicktome")

from myninjas.settings import INIConfig
from myninjas.utils import write_file
import os
from quicktome.library.files import ProcessOutline
from quicktome.library.projects import Project
from slugify import slugify

# Create the roles.markdown file from the roles.ini file.
config = INIConfig(os.path.join("source", "content", "_data", "roles.ini"))
config.load()

lines = list()
lines.append("# Roles")
lines.append("")

for section in config.get_sections():
    lines.append("## %s" % section.title)
    lines.append("")
    lines.append(section.description)
    lines.append("")

path = os.path.join("source", "content", "roles.markdown")
print("\n".join(lines))
write_file(path, "\n".join(lines))

# Parse the process outline to create processes.markdown file.
project = Project(os.getcwd())
outline = ProcessOutline(os.path.join("source", "content", "processes.opml"), project)
outline.load()
print(outline.processes)

lines = list()
lines.append("# Processes")
lines.append("")

for process in outline.get_all_processes():
    slug = slugify(process.title, unique=True).lower()
    process.slug = slug

    path = os.path.join("source", "content", "processes", slug + ".markdown")
    write_file(path, process.to_markdown())

    lines.append(".. include:: processes/%s" % slug + ".markdown")
    lines.append("")

path = os.path.join("source", "content", "processes.markdown")
write_file(path, "\n".join(lines))

# Create the glossary.markdown file from the definitions.ini file.
config = INIConfig(os.path.join("source", "content", "_data", "definitions.ini"))
config.load()

lines = list()
lines.append("# Glossary")
lines.append("")

for section in config.get_sections():
    lines.append(section.title)
    lines.append(":    %s" % section.definition)
    lines.append("")

path = os.path.join("source", "content", "glossary.markdown")
write_file(path, "\n".join(lines))

# Imports

from datetime import datetime
from jinja2.exceptions import UndefinedError as JinjaUndefinedError
import logging
from myninjas.feedback import Feedback
from myninjas.shell import Command
from myninjas.tables import Table
from myninjas.utils import copy_file, parse_jinja_template, read_file, truncate, write_file, File
import os
# noinspection PyPackageRequirements
from slugify import slugify
from ..library.archetypes import Archetype, Skeleton as SkeletalStructure
from ..library.outputs import MAPPING as OUTPUT_MAPPING
from ..library.projects import NewProjectForm
from ..library.registry import registry
from ..library.settings import ProjectConfig
from ..library.staging import Staging
from ..constants import INPUT_FORMAT_MD, INPUT_LOADER_MANIFEST, INPUT_LOADER_OUTLINE, \
    INPUT_LOADER_TREE, OUTPUT_PDF, SKELETON_CHOICES
from ..variables import LOGGER_NAME, PROJECT_HOME, QUICKTOME_PATH
from .initialize import initialize_config, initialize_context, initialize_project

log = logging.getLogger(LOGGER_NAME)

# Exports

__all__ = (
    "get_archetypes",
    "get_input_file_name",
    "get_projects",
    "get_templates",
    "Build",
    "ListInventory",
    "ListProjects",
    "ListTodos",
    "Preflight",
    "Stats",
)

# Helpers


def get_archetypes(project):
    """Get content models included in the project.

    :param project: The current project instance.
    :type project: Project

    :rtype: list[Archetype]

    """
    path = os.path.join(project.root, "source", "archetypes")
    if not os.path.exists(path):
        log.warning("Archetypes path does not exist: %s" % path)
        return list()

    a = list()
    for p in os.listdir(path):
        f = Archetype(os.path.join(path, p))
        if f.extension == ".ini":
            continue

        a.append(f)

    return a


def get_input_file_name(config, prefix=None, suffix=None):
    """Get the name of the input file used to generate the final output.

    :param config: The configuration instance.
    :type config: Config

    :param prefix: The file prefix, if any.
    :type prefix: str

    :param suffix: The file suffix, if any.
    :type suffix: str

    :rtype: str

    """
    if config.has("input", "name"):
        input_file = config.input.name
    else:
        input_file = slugify(config.project.title)

    if prefix is not None:
        input_file = "%s-%s" % (slugify(prefix), input_file)

    if suffix is not None:
        input_file = "%s-%s" % (input_file, slugify(suffix))

    input_file += config.input.extension

    return input_file


def get_projects(path):
    """Get a list of QuickTome projects.

    :param path: The path to search.
    :type path: str

    :rtype: list[Config] | None

    """
    if not os.path.exists(path):
        log.error("Projects path does not exist: %s" % path)
        return None

    # Get the entries from the projects directory. This will raise an OSError if the path does not exist.
    entries = os.listdir(path)
    projects = list()
    for e in entries:
        if e[0] == ".":
            continue

        marker = os.path.join(path, e, ".quicktome")
        if not os.path.exists(marker):
            continue

        project = ProjectConfig(os.path.join(path, e, "meta.ini"))
        project.load()

        projects.append(project)

    return projects


def get_templates(project):
    """Get output templates included in the project.

    :param project: The current project instance.
    :type project: Project

    :rtype: list[File]

    """
    path = os.path.join(project.root, "source", "templates")
    if not os.path.exists(path):
        log.warning("Templates path does not exist: %s" % path)
        return list()

    a = list()
    for root, dirs, files in os.walk(path):
        for file_name in files:
            f = File(os.path.join(root, file_name))

            a.append(f)

    return a


class SubCommand(object):
    """Base class for implementing sub-commands."""

    def __init__(self, args):
        """Initialze the sub-command.

        :param args: The command line arguments.
        :type args: Namespace

        """
        self.args = args
        self.config = None
        self.context = None
        self.is_loaded = False
        self.project = None

        self.initialize()

    def initialize(self):
        # Not that logging occurs in each of the initialize_ functions so there's no need to do any logging here.
        self.project = initialize_project(self.args)
        if self.project is None:
            return False

        self.config = initialize_config(self.project)
        self.context = initialize_context(self.args, self.config)

        self.is_loaded = True

        return True

    def run(self):
        """Run the command.

        :rtype: bool

        """
        raise NotImplementedError()


# Commands


class Build(SubCommand):
    """Build and output content in a specific export format."""

    def run(self):
        # Load all content files into the registry.
        registry.find_all_files(os.path.join(self.project.root, "source", "content"))

        # Run before load content hook.
        self._run_before_load_content()

        # Load content, images, etc.
        loader = self.project.get_loader(self.config, context=self.context, output_format=self.args.output_format)
        if not loader.load():
            return False

        log.info("Content files found: %s" % loader.count)

        # Initialize staging instance.
        staging = Staging(self.config, loader, self.project)

        # Prepare to pass pandoc options.
        pandoc_options = dict()
        if self.args.highlight_style:
            pandoc_options['highlight_style'] = self.args.highlight_style

        # Prepare for output. Creates directories, copies assets and templates.
        output = staging.prepare_output(self.args.output_format, **pandoc_options)

        if "include_index" in self.args and self.args.include_index and self.args.output_format == OUTPUT_PDF:
            output.include_index = True

        # Prepare input. Copy images to the staging directory. Copy the bibliography file if it exists.
        staging.prepare_input(self.args.output_format)

        # Run before export hook.
        self._run_before_export()

        # Convert the content.
        command = output.get_command()



        if self.args.preview_enabled:
            log.info(command.preview())
            self._run_after_export()
        else:
            log.info("Converting content to: %s" % self.args.output_format)
            log.debug(command.preview())
            output.write()
            self._run_after_export()

        # List unused files.
        if self.args.show_unused:
            for p in registry.get_unused():
                log.warning("Unused content file: %s" % p)

            for p in registry.get_unused(file_type="data"):
                log.warning("Unused data file: %s" % p)

            for p in registry.get_unused(file_type="image"):
                log.warning("Unused image file: %s" % p)

        return True

    def _run_after_export(self):
        """Execute the ``after_export.py`` script, if it exists.

        :rtype: bool

        """
        if self.project.path_exists("source", "hooks", "after_export.py"):
            script = os.path.join("source", "hooks", "after_export.py")
            log.info("Running after_export hook: %s" % script)
            command = Command("./%s" % script, path=self.project.root)

            if not self.args.preview_enabled:
                if not command.run():
                    log.warning("Failed to run after_export script.")
                    log.warning(command.error)
                    return False

        return True

    def _run_before_export(self):
        """Execute the ``before_export.py`` script, if it exists.

        :rtype: bool

        """
        if self.project.path_exists("source", "hooks", "before_export.py"):
            script = os.path.join("source", "hooks", "before_export.py")
            log.info("Running before_export hook: %s" % script)
            command = Command("./%s" % script, path=self.project.root)

            if not self.args.preview_enabled:
                if not command.run():
                    log.warning("Failed to run before_export script.")
                    log.warning(command.error)
                    return False

        return True

    def _run_before_load_content(self):
        """Execute the ``before_load_content.py`` script, if it exists.

        :rtype: bool

        """
        script = os.path.join("source", "hooks", "before_load_content.py")
        log.debug("before_load_content hook is: %s" % script)
        if self.project.path_exists(script):
            log.info("Running before_load_content hook: %s" % script)
            command = Command("./%s" % script, path=self.project.root)

            if not self.args.preview_enabled:
                if not command.run():
                    log.warning("Failed to run before_load_content script.")
                    log.warning(command.error)
                    return False

        return True

    def run1(self):

        # Run before build hooks.
        if self.project.path_exists("source", "hooks", "before_build.py"):
            log.info("Running before_build hook.")
            script = os.path.join("source", "hooks", "before_build.py")
            command = Command("./%s" % script, path=self.project.root)

            if not self.args.preview_enabled:
                if not command.run():
                    log.warning("Failed to run before_build script.")
                    log.warning(command.error)

        # Prep the staging directory.
        staging_path = self.project.create_path("temporary")

        # Determine which method will be  used to find content files.
        loader = self.project.get_loader(self.config, context=self.context, output_format=self.args.output_format)
        if not loader.load():
            return False

        log.info("Content files to be exported: %s" % loader.count)

        # Copy images to the staging directory.
        log.info("Copying image files to staging directory.")
        loader.collect_image_files(
            staging_path,
            image_directory=self.config.images.directory,
            preview=self.args.preview_enabled
        )

        # Copy templates and such to the staging directory.
        log.info("Copying template files to staging directory.")
        loader.collect_template_files(staging_path, preview=self.args.preview_enabled)

        # Copy bibliography file.
        if loader.has_bibliography_file:
            log.info("Copying bibliography file to the staging directory.")
            if not self.args.preview_enabled:
                loader.copy_bibliography_file()

        # Compile content files into a single input file in the staging directory.
        input_file = get_input_file_name(self.config, prefix=self.args.prefix, suffix=self.args.suffix)
        log.debug("Input file is: %s" % input_file)
        loader.collect_content_files(input_file, staging_path, preview=self.args.preview_enabled)

        # Create the slideshow file. This must be done *after* content collection so slideshow directives may be
        # discovered.
        log.info("Generating slideshow content.")
        if not self.args.preview_enabled:
            loader.export_slideshow()

        # Run the pandoc conversion/export command to generate the output file.
        output_file = input_file.replace(self.config.input.extension, "")
        log.debug("Output file is: %s" % output_file)
        output_class = OUTPUT_MAPPING[self.args.output_format]
        output = output_class(
            output_file,
            self.project.root,
            input_extension=self.config.input.extension,
            input_format=self.config.input.format
        )

        command = output.get_command()
        if self.args.preview_enabled:
            print(command.preview())
            success = True
        else:
            output.preprocess()

            log.info("Running post-processing for output: %s" % self.args.output_format)
            if not command.run():
                log.warning(command.error)

            success = False

        # Run after build hooks.
        if self.project.path_exists("source", "hooks", "after_build.py"):
            log.info("Running after_build hook.")
            script = os.path.join("source", "hooks", "after_build.py")
            command = Command("./%s" % script, path=self.project.root)

            if not self.args.preview_enabled:
                if not command.run():
                    log.warning("Failed to run after_build script.")
                    log.warning(command.error)
                    success = False

        return success


class Init(SubCommand):

    def initialize(self):
        """Don't set up resources because they don't exist yet."""
        self.is_loaded = True
        return True

    def run(self):
        # Make these variables easier to use.
        preview = self.args.preview_enabled
        project_root = self.args.project_root

        # Create project
        if not os.path.exists(project_root):
            log.info("Creating project root directory: %s" % project_root)

            if not preview:
                os.makedirs(project_root)

        # Gather user input.
        form = self._prompt()

        a = list()
        a.append("[project]")
        a.append("description = %s" % form.get("description"))
        a.append("edition = %s" % form.get("edition", 1))
        a.append("title = %s" % form.get("title"))
        a.append("")

        a.append("[author]")
        a.append("name = %s" % form.get("author_name"))
        a.append("title = %s" % form.get("author_title"))
        a.append("")

        a.append("[copyright]")
        a.append("name = %s" % form.get("copyright_name"))
        a.append("year = %s" % datetime.now().year)
        a.append("")

        a.append("[input]")
        a.append("directory = temporary")
        a.append("extension = %s" % form.get("input_extension", ".markdown"))
        a.append("format = %s" % form.get("input_format", "markdown"))
        a.append("loader = %s" % form.get("input_loader", INPUT_LOADER_TREE))
        a.append("name = %s" % slugify(form.values['title']))
        a.append("")

        # Output options are added, but commented out.
        for output_class in OUTPUT_MAPPING.values():
            a.append(";[%s]" % output_class.__name__.lower())
            for key, value in output_class.get_meta_values().items():
                a.append(";%s = %s" % (key, value))

            a.append("")

        # Write the meta.ini file.
        path = os.path.join(project_root, "meta.ini")
        log.info("Writing meta file: %s" % path)
        if preview:
            print("%s meta.ini %s" % ("-" * 34, "-" * 34))
            print("\n".join(a))
            print("-" * 80)
        else:
            write_file(path, "\n".join(a))

        # Create directory structure.
        self._create_directory_structure()

        # Create the QuickTome marker.
        path = os.path.join(project_root, ".quicktome")
        if not os.path.exists(path):
            log.info("Create the quicktome marker file: %s" % path)

            content = "This file is used by to identify QuickTome projects."
            if not preview:
                write_file(path, content=content)

        # Copy starter files for archetypes.
        self._copy_archetype_files(form.get("input_extension"))

        # Copy template files for generating output.
        self._copy_template_files()

        # Copy loader template.
        self._copy_loader_files(form.get("input_loader"))

        # And ... we're done.
        return True

    def _copy_archetype_files(self, input_extension):
        locations = [
            os.path.join(QUICKTOME_PATH, "templates", "archetypes", "chapter.ini"),
            os.path.join(QUICKTOME_PATH, "templates", "archetypes", "chapter.markdown"),
            os.path.join(QUICKTOME_PATH, "templates", "archetypes", "contact.markdown"),
            os.path.join(QUICKTOME_PATH, "templates", "archetypes", "part_N.markdown"),
            os.path.join(QUICKTOME_PATH, "templates", "archetypes", "title.markdown"),
        ]
        for from_path in locations:
            to_path = os.path.join(self.args.project_root, "source", "archetypes", os.path.basename(from_path))
            to_path = to_path.replace(".markdown", input_extension)
            if not os.path.exists(to_path):
                log.info("Copying archetype starter file: %s" % os.path.basename(from_path))

                if not self.args.preview_enabled:
                    copy_file(from_path, to_path)

    def _copy_loader_files(self, input_loader):
        if input_loader == INPUT_LOADER_MANIFEST:
            from_path = os.path.join(QUICKTOME_PATH, "templates", "loaders", "MANIFEST.txt")
            to_path = os.path.join(self.args.project_root, "source", "MANIFEST.txt")

            if not self.args.preview_enabled:
                copy_file(from_path, to_path)
        elif input_loader == INPUT_LOADER_OUTLINE:
            from_path = os.path.join(QUICKTOME_PATH, "templates", "loaders", "outline.ooutline")
            to_path = os.path.join(self.args.project_root, "source", "outline.ooutline")

            if not self.args.preview_enabled:
                copy_file(from_path, to_path)

            from_path = os.path.join(QUICKTOME_PATH, "templates", "loaders", "outline.opml")
            to_path = os.path.join(self.args.project_root, "source", "outline.opml")

            if not self.args.preview_enabled:
                copy_file(from_path, to_path)
        else:
            pass

    def _copy_template_files(self):
        locations = [
            os.path.join(QUICKTOME_PATH, "templates", "outputs", "docx", "default.docx"),
            os.path.join(QUICKTOME_PATH, "templates", "outputs", "epub", "epub.css"),
            os.path.join(QUICKTOME_PATH, "templates", "outputs", "html", "footer.html"),
            os.path.join(QUICKTOME_PATH, "templates", "outputs", "html", "header.html"),
            os.path.join(QUICKTOME_PATH, "templates", "outputs", "pdf", "default.tex"),
            os.path.join(QUICKTOME_PATH, "templates", "outputs", "printable", "default.tex"),
        ]
        for from_path in locations:
            directory_name = os.path.basename(os.path.dirname(from_path))
            file_name = os.path.basename(from_path)
            to_path = os.path.join(self.args.project_root, "source", "templates", directory_name, file_name)

            if not os.path.exists(to_path):
                log.info("Copying output template file: %s" % os.path.basename(file_name))

                if not self.args.preview_enabled:
                    copy_file(from_path, to_path)

    def _create_directory_structure(self):
        locations = [
            os.path.join(self.args.project_root, "assets"),
            os.path.join(self.args.project_root, "output"),
            os.path.join(self.args.project_root, "source"),
            os.path.join(self.args.project_root, "source", "archetypes"),
            os.path.join(self.args.project_root, "source", "content"),
            os.path.join(self.args.project_root, "source", "content", "_data"),
            os.path.join(self.args.project_root, "source", "content", "_images"),
            os.path.join(self.args.project_root, "source", "hooks"),
            os.path.join(self.args.project_root, "source", "settings"),
            os.path.join(self.args.project_root, "source", "templates"),
            os.path.join(self.args.project_root, "source", "templates", "docx"),
            os.path.join(self.args.project_root, "source", "templates", "epub"),
            os.path.join(self.args.project_root, "source", "templates", "html"),
            os.path.join(self.args.project_root, "source", "templates", "pdf"),
            os.path.join(self.args.project_root, "source", "templates", "printable"),
            os.path.join(self.args.project_root, "temporary"),
        ]
        for location in locations:
            if not os.path.exists(location):
                log.info("Creating directory: %s" % location)

                if not self.args.preview_enabled:
                    os.makedirs(location)

    def _prompt(self):
        """Prompt the user for input."""
        form = NewProjectForm()

        if self.args.empty:
            form.values['author_name'] = "Unknown"
            form.values['author_title'] = "Unspecified"
            form.values['copyright_name'] = "ACME, Inc."
            form.values['description'] = "Describe the project."
            form.values['edition'] = 1
            form.values['input_extension'] = ".markdown"
            form.values['input_format'] = INPUT_FORMAT_MD
            form.values['input_loader'] = INPUT_LOADER_MANIFEST
            form.values['title'] = "Untitled Project"
        elif self.args.preview_enabled:
            form.values['author_name'] = "Wile E. Coyote"
            form.values['author_title'] = "Carnivorus Vulgaris"
            form.values['copyright_name'] = "ACME, Inc."
            form.values['description'] = "This is a preview."
            form.values['edition'] = 1
            form.values['input_extension'] = ".markdown"
            form.values['input_format'] = INPUT_FORMAT_MD
            form.values['input_loader'] = INPUT_LOADER_TREE
            form.values['title'] = "Project Preview"
        else:
            form.prompt()

        return form


class ListComments(SubCommand):
    """List the comments found in content."""

    def initialize(self):
        logging.disable()

        self.project = initialize_project(self.args)
        if self.project is None:
            print("Project does not exist: %s" % self.args.project_root)
            return False

        self.config = initialize_config(self.project)

        self.is_loaded = True

        return True

    def run(self):
        if not self.is_loaded:
            return False

        # Determine which method will be used to find content files.
        loader = self.project.get_loader(self.config)
        if not loader.load():
            return False

        # Load content files to discover comments.
        staging = Staging(self.config, loader, self.project)
        staging.collect_content_files()

        # Export comments that were found during processing.
        comments = loader.get_comments()
        for c in comments:
            print(c.content)
            print("%s:%s" % (c.path, c.line_number))
            print("")

        return True


class ListInventory(SubCommand):
    """List content in the current project."""

    def initialize(self):
        logging.disable()
        return super().initialize()

    def run(self):

        # Determine which method will be  used to find content files.
        loader = self.project.get_loader(self.config, context=self.context)
        if not loader.load():
            return False

        # Get archetypes.
        if self.args.display_archetypes:
            archetypes = get_archetypes(self.project)

            headings = [
                "File Name",
                "Type",
                "Location",
                "Takes Context",
            ]
            table = Table(headings=headings, formatting=self.args.output_format)

            for f in archetypes:
                file_type = f.extension.replace(".", "")
                location = f.directory.replace(self.project.root, "")

                if f.has_meta:
                    takes_context = "Yes"
                else:
                    takes_context = "No"

                table.add([f.name, file_type, location, takes_context])

            print(table)

        # Get images.
        if self.args.display_images:
            # print("Image Files")
            # print("-" * 120)

            images = loader.get_image_files()

            headings = [
                "File Name",
                "Type",
                "Location",
            ]
            table = Table(headings=headings, formatting=self.args.output_format)

            for f in images:
                file_type = f.extension.replace(".", "")
                location = f.directory.replace(self.project.root, "")
                table.add([f.name, file_type, location])

            print(table)

        # Get content files.
        if self.args.display_content:
            files = loader.get_content_files()

            headings = [
                "File Name",
                "Type",
                "Location",
            ]
            table = Table(headings=headings, formatting=self.args.output_format)

            for f in files:
                file_type = f.extension.replace(".", "")
                location = f.directory.replace(self.project.root, "")
                table.add([f.name, file_type, location])

            print(table)

        # Get templates.
        if self.args.display_templates:
            templates = get_templates(self.project)

            headings = [
                "File Name",
                "Type",
                "Location",
                "Output Format",
            ]
            table = Table(headings=headings, formatting=self.args.output_format)

            for f in templates:
                file_type = f.extension.replace(".", "")
                location = f.directory.replace(self.project.root, "")
                output_format = os.path.basename(f.directory)

                table.add([f.name, file_type, location, output_format])

            print(table)

        return True


class ListProjects(SubCommand):
    """List QuickTome projects."""

    def initialize(self):
        self.is_loaded = True
        return True

    def run(self):
        headings = [
            "Title",
            "Author",
            "Edition",
            "Copyright",
        ]

        table = Table(formatting=self.args.output_format, headings=headings)

        projects = get_projects(PROJECT_HOME)
        for p in projects:
            title = truncate(p.project.title)

            values = [
                title,
                p.author.name,
                p.project.edition,
                p.copyright.year,
            ]
            table.add(values)

        print("")
        print(table)
        print("")

        return True


class ListTodos(SubCommand):
    """List the to-dos found in content."""

    def initialize(self):
        logging.disable()

        self.project = initialize_project(self.args)
        if self.project is None:
            print("Project does not exist: %s" % self.args.project_root)
            return False

        self.config = initialize_config(self.project)

        self.is_loaded = True

        return True

    def run(self):
        if not self.is_loaded:
            return False

        # Determine which method will be used to find content files.
        loader = self.project.get_loader(self.config)
        if not loader.load():
            return False

        # Load content files to discover todos.
        staging = Staging(self.config, loader, self.project)
        staging.collect_content_files()

        # Export todos that were found during processing.
        todos = loader.get_todos()
        for t in todos:
            print(t.content)
            print("%s:%s" % (t.path, t.line_number))
            print("")

        return True


class New(SubCommand):
    """Create new content."""

    def run(self):
        # Load the archetype.
        from_path = os.path.join(
            self.project.root,
            "source",
            "archetypes",
            self.args.archetype + self.config.input.extension
        )
        log.debug("Full from path: %s" % from_path)

        archetype = Archetype(from_path)
        if not archetype.exists:
            log.error("Archetype does not exist: %s" % archetype.name)
            return False

        # Prepare the context.
        context = None
        if archetype.has_meta:
            context = self.context.mapping()
            form = archetype.get_form()
            form.prompt()

            context.update(form.values)

        # Create the file.
        to_path = os.path.join(
            self.project.root,
            "source",
            "content",
            self.args.path + self.config.input.extension
        )
        log.debug("Full to path: %s" % to_path)

        log.info("Creating %s from %s archetype." % (to_path, os.path.basename(from_path)))
        try:
            content = parse_jinja_template(from_path, context)
        except JinjaUndefinedError as e:
            log.error("Failed to parse archetype: %s" % e)
            return False

        if self.args.preview_enabled:
            print(content)
        else:
            write_file(to_path, content=content, make_directories=True)

        return True


class Preflight(SubCommand):
    """Preflight the project for potential problems, prior to build."""

    def run(self):
        feedback = Feedback()
        feedback.heading("Utility Check")

        # Check for the necessary system utilities.
        # name, version, required, description, URL
        utils = [
            ("pandoc", "1.19+", True, "Pandoc is used to convert input files to various formats "),
            ("make", None, False, "Make may be used to automate build commands using the Makefile."),
            ("kindlegen", None, False, "The Kindle Generator is required for creating mobi output."),
        ]
        for name, version, required, description in utils:
            command = Command("which %s" % name)
            exists = command.run()

            if exists:
                feedback.green(name)
            elif required and not exists:
                feedback.red(name, suffix="<-- required")
            else:
                feedback.yellow(name, suffix="<-- optional")

            feedback.plain(description)
            feedback.cr()

        # Make sure templates are defined.
        feedback.heading("Output Templates")
        locations = [
            os.path.join(self.args.project_root, "source", "templates", "default.tex"),
            os.path.join(self.args.project_root, "source", "templates", "epub.css"),
            os.path.join(self.args.project_root, "source", "templates", "footer.html"),
            os.path.join(self.args.project_root, "source", "templates", "header.html"),
            os.path.join(self.args.project_root, "source", "templates", "printable.tex"),
            os.path.join(self.args.project_root, "source", "templates", "reference.docx"),
        ]
        for location in locations:
            file = File(location)
            if file.exists:
                feedback.green(file.basename)
            else:
                feedback.yellow(file.basename, suffix="<-- missing")

        feedback.cr()

        print(feedback)

        return True


class Skeleton(SubCommand):
    """Build a project's structure."""

    def run(self):
        if self.args.bones == "?":
            keys = sorted(SKELETON_CHOICES.keys())
            for k in keys:
                s = SkeletalStructure(k, description=SKELETON_CHOICES[k], project=self.project)
                print(s.to_console())
                print("")
                # print("%s: %s" % (k, SKELETON_CHOICES[k]))
                # print("")

            return True

        if self.args.skeleton == "manifest":
            log.info("Building structure based on MANIFEST.txt entries.")
            loader = self.project.get_loader(self.config, self.context)
            files = loader.get_content_files()
            for f in files:
                path = os.path.join(self.project.root, "source", "content", f.path + self.config.input.extension)
                if not os.path.exists(path):
                    log.info("Creating file: %s" % path)
                    if not self.args.preview_enabled:
                        write_file(path, make_directories=True)
        else:
            log.info("Building structure using skeleton: %s" % self.args.skeleton)

            name = self.args.skeleton
            structure = SkeletalStructure(name, description=SKELETON_CHOICES[name], project=self.project)
            if not structure.exists:
                log.warning("Skeleton does not exist: %s" % name)
                return False

            members = structure.get_structure()
            for member in members:
                if member.to_path_exists:
                    log.debug("Member already exists: %s" % member.to_path)
                    continue

                if not self.args.preview_enabled:
                    member.copy()

        return True


class Stats(SubCommand):
    """Print project statistics."""

    def run(self):
        # Prep the staging directory.
        staging_path = self.project.create_path("temporary")

        # Determine which method will be  used to find content files.
        loader = self.project.get_loader(context=self.context)
        loader.load()

        # Get templates.
        templates = loader.get_template_files()

        # Get images.
        images = loader.get_image_files()

        # Get the name of the master input file.
        input_file = "%s%s" % (slugify(self.config.project.title), self.config.input.extension)

        # Build the content.
        loader.compile_content(input_file, staging_path)

        # Count the words in the file.
        content = read_file(os.path.join(staging_path, input_file))
        lines = content.split("\n")
        word_count = 0
        for line in lines:
            if len(line) == 0:
                continue

            words = line.split(" ")
            for word in words:
                if word.startswith("\\"):
                    continue

                word_count += 1

        print("")
        print("=" * 80)
        print(self.config.project.title)
        print("=" * 80)
        print("Report")
        print("-" * 80)
        print("")
        print("Master Input File: %s" % input_file)
        print("Content Files: %s" % loader.count)
        print("Words: %s" % word_count)
        print("Images: %s" % len(images))
        print("Templates: %s" % len(templates))
        print("-" * 80)
        print("")

        return True

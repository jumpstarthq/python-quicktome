# Imports

from argparse import ArgumentParser, RawDescriptionHelpFormatter
import os
from myninjas.logging import LoggingHelper
from myninjas.shell import EXIT_FAILURE, EXIT_SUCCESS
from .initialize import initialize_subcommands
from . import subcommands
from ..variables import LOGGER_NAME

DEBUG = 10

logging_helper = LoggingHelper(LOGGER_NAME)
log = logging_helper.setup()

# TODO: Prompt for choice of Markdown or RST on project init.

# Commands


def quicktome_command():
    """Run quicktome sub-commands."""

    __author__ = "Shawn Davis <shawn@jumpstarthq.com>"
    __date__ = "2019-01-05"
    __help__ = """NOTES

The quicktome command is the entry point for working with content.

    """
    __version__ = "0.14.2-a"

    parser = ArgumentParser(description=__doc__, epilog=__help__, formatter_class=RawDescriptionHelpFormatter)

    subparsers = parser.add_subparsers(
        dest="subcommand",
        help="Sub Commands",
        metavar="build, comments, init, inv, ls, new, preflight, skeleton, stats, todos"
    )

    initialize_subcommands(subparsers)

    # Access to the version number requires special consideration, especially
    # when using sub parsers. The Python 3.3 behavior is different. See this
    # answer: http://stackoverflow.com/questions/8521612/argparse-optional-subparser-for-version
    # parser.add_argument('--version', action='version', version='%(prog)s 2.0')
    parser.add_argument(
        "-v",
        action="version",
        help="Show version number and exit.",
        version=__version__
    )
    parser.add_argument(
        "--version",
        action="version",
        help="Show verbose version information and exit.",
        version="%(prog)s" + " %s %s by %s" % (__version__, __date__, __author__)
    )

    args = parser.parse_args()
    # print(args)
    command_name = args.subcommand

    # Enable debug mode.
    if args.debug_enabled:
        log.level = DEBUG
        log.debug("Command line arguments: %s" % args)

    # Run the sub-command.
    if command_name == "build":
        command = subcommands.Build(args)
        success = command.run()
    elif command_name == "comments":
        command = subcommands.ListComments(args)
        success = command.run()
    elif command_name == "init":
        command = subcommands.Init(args)
        success = command.run()
    elif command_name == "inv":
        command = subcommands.ListInventory(args)
        success = command.run()
    elif command_name == "ls":
        command = subcommands.ListProjects(args)
        success = command.run()
    elif command_name == "new":
        command = subcommands.New(args)
        success = command.run()
    elif command_name == "preflight":
        command = subcommands.Preflight(args)
        success = command.run()
    elif command_name in ("skel", "skeleton"):
        command = subcommands.Skeleton(args)
        success = command.run()
    elif command_name == "stats":
        command = subcommands.Stats(args)
        success = command.run()
    elif command_name == "todos":
        command = subcommands.ListTodos(args)
        success = command.run()
    else:
        print("NOT IMPLEMENTED: %s" % command_name)
        success = False

    # Send exit code.
    if success:
        exit(EXIT_SUCCESS)
    else:
        exit(EXIT_FAILURE)

# Imports

from datetime import datetime
import logging
from myninjas.contexts import Context
from myninjas.tables.constants import OUTPUT_FORMAT_COMMON_CHOICES as TABLE_FORMAT_CHOICES, OUTPUT_FORMAT_SIMPLE as \
    DEFAULT_TABLE_FORMAT
from myninjas.utils import read_file
import os
from ..constants import OUTPUT_CHOICES, OUTPUT_PRINTABLE
from ..library.projects import Project
from ..library.settings import ExtraVariablesConfig, ProjectConfig
from ..variables import LOGGER_NAME

log = logging.getLogger(LOGGER_NAME)

# Exports

__all__ = (
    "initialize_config",
    "initialize_context",
    "initialize_project",
    "initialize_subcommands",
    "SubCommands",
)

# Constants

DEFAULT_PROJECT_ROOT = os.getcwd()

# Functions


def initialize_config(project):
    """Initialize settings from the ``config.ini`` file.

    :param project: The project instance.
    :type project: Project

    :rtype: Config

    """
    config_path = project.get_path("meta.ini")
    config = ProjectConfig(config_path)

    if not config.exists:
        log.warning("Missing configuration file: %s" % config_path)
        config.load()
        return config

    if config.load():
        log.info("Loaded configuration from: %s" % config_path)
    else:
        log.warning("Failed to load configuration from: %s" % config_path)

    return config


def initialize_context(args, config):
    """Initialize the global context.

    :param args: The command line args.
    :type args: Namespace

    :param config: The current configuration instance.
    :type config: Config

    :rtype: Context

    """
    # Initialize the context.
    log.info("Initializing global context.")
    context = Context("global")

    # Add project root.
    context.add("project_root", args.project_root)

    # Add the output_format when build is invoked.
    if args.subcommand == "build":
        context.add("output_format", args.output_format)

    # Add the current date and time.
    current_time = datetime.now()
    context.add("current_month", current_time.month)
    context.add("current_month_name", current_time.strftime("%B"))
    context.add("current_time", current_time)
    context.add("current_year", current_time.year)

    # Add the current version/release string.
    version_path = os.path.join(args.project_root, "VERSION.txt")
    if os.path.exists(version_path):
        context.add("current_version", read_file(version_path).strip())
    else:
        context.add("current_version", "0.1.0-d")

    # Add configuration sections to the global context.
    for section in config.get_sections():
        context.add(section.get_name(), section)

    # Add "extra" variables.
    if "extra_variables" in args and args.extra_variables is not None:
        if os.path.exists(args.extra_variables):
            log.info("Loading extra variables from: %s" % args.extra_variables)
            extras = ExtraVariablesConfig(args.extra_variables)
            extras.load()
            print("advanced" in extras.rulesets)
            for section in extras.get_sections():
                context.add(section.get_name(), section)

    # Add variables from the command line to the global context.
    if "variables" in args and args.variables is not None:
        for v in args.variables:
            key, value = v.split(":")
            value = value.replace('"', "")
            context.add(key, value)

    log.debug("Global context: %s" % context)

    return context


def initialize_project(args):
    """Initialize the project.

    :param args: The command line args.
    :type args: Namespace

    :rtype: Project

    """
    # Set the project root and check for required directories.
    if args.project_root:
        project_root = os.path.abspath(args.project_root)
    else:
        project_root = os.path.abspath(os.getcwd())

    log.debug("Project root: %s" % project_root)

    source_path = os.path.join(project_root, "source")
    if not os.path.exists(source_path) and args.subcommand != "init":
        log.error("Project does not exist: %s" % source_path)
        return None

    content_path = os.path.join(source_path, "content")
    if not os.path.exists(content_path) and args.subcommand != "init":
        log.error("Content path does not exist: %s" % content_path)
        return None

    # Initialize the project instance.
    return Project(project_root)


def initialize_subcommands(subparsers):
    """Shortcut function for initializing a SubCommands instance."""
    commands = SubCommands(subparsers)
    commands.build()
    commands.comments()
    commands.init()
    commands.inv()
    commands.ls()
    commands.new()
    commands.preflight()
    commands.skeleton()
    commands.stats()
    commands.todos()

# Classes


class SubCommands(object):
    """A utility class which keeps the ``cli`` package clean."""

    def __init__(self, subparsers):
        self.subparsers = subparsers

    def build(self):
        """Initialize the build sub-command."""
        sub = self.subparsers.add_parser(
            "build",
            help="Build the output of the project."
        )

        sub.add_argument(
            "output_format",
            choices=OUTPUT_CHOICES,
            default=OUTPUT_PRINTABLE,
            # dest="output_format",
            help="Specify the output format for compiled content."
        )

        sub.add_argument(
            "-e=",
            "--extras=",
            dest="extra_variables",
            help="The path to an INI file that includes extra variables to include in the context."
        )

        sub.add_argument(
            "--highlight-style=",
            dest="highlight_style",
            help="Highlight code using this style. Tip: To see available styles, use pandoc --highlight-styles"
        )

        sub.add_argument(
            "-i",
            "--index",
            action="store_true",
            dest="include_index",
            help="Generate an index at the end of the file. Currently only supported by the pdf output format."
        )

        sub.add_argument(
            "-N",
            "--no-context",
            action="store_true",
            dest="context_disabled",
            help="Do not pass context variables to the build system."
        )

        sub.add_argument(
            "--prefix=",
            dest="prefix",
            help="Optional prefix for the input file name."
        )

        sub.add_argument(
            "--suffix=",
            dest="suffix",
            help="Optional suffix for the input file name."
        )

        sub.add_argument(
            "-U=",
            "--unused=",
            action="store_true",
            dest="show_unused",
            help="Show content, images, or data files that have not been included in the build."
        )

        sub.add_argument(
            "-V=",
            "--var=",
            action="append",
            dest="variables",
            help="Add a variable to the global context in the form of name:value."
        )

        self._add_common_options(sub)

    def comments(self):
        """Initialize the comments sub-command."""
        sub = self.subparsers.add_parser(
            "comments",
            help="List comments found in content."
        )

        self._add_common_options(sub)

    def init(self):
        """Initialize the init sub-command."""
        sub = self.subparsers.add_parser(
            "init",
            help="Initialize a new project."
        )

        sub.add_argument(
            "-e",
            "--empty",
            action="store_true",
            dest="empty",
            help="Initialize an empty project"
        )

        self._add_common_options(sub)

    def inv(self):
        """Initialize the inv sub-command."""
        sub = self.subparsers.add_parser(
            "inv",
            help="List content in the current project."
        )

        sub.add_argument(
            "-A",
            "--archetype",
            action="store_true",
            dest="display_archetypes",
            help="Display archetype files."
        )

        sub.add_argument(
            "-c",
            "--content",
            action="store_true",
            dest="display_content",
            help="Display content files."
        )

        sub.add_argument(
            "-i",
            "--images",
            action="store_true",
            dest="display_images",
            help="Display images."
        )

        sub.add_argument(
            "-O=",
            "--output-format=",
            choices=TABLE_FORMAT_CHOICES,
            default=DEFAULT_TABLE_FORMAT,
            dest="output_format",
            help="The tabulate output format."
        )

        sub.add_argument(
            "-t",
            "--templates",
            action="store_true",
            dest="display_templates",
            help="Display output templates."
        )

        self._add_common_options(sub)

    def ls(self):
        """Initialize the init sub-command."""
        sub = self.subparsers.add_parser(
            "ls",
            help="List QuickTome projects."
        )

        sub.add_argument(
            "-D",
            "--debug",
            action="store_true",
            dest="debug_enabled",
            help="Enable debug mode. Produces extra output."
        )

        sub.add_argument(
            "-O=",
            "--output-format=",
            choices=TABLE_FORMAT_CHOICES,
            default=DEFAULT_TABLE_FORMAT,
            dest="output_format",
            help="The tabulate output format."
        )

    def new(self):
        """Initialize the new sub-command."""
        sub = self.subparsers.add_parser(
            "new",
            help="Add new content to the project."
        )

        sub.add_argument(
            "path",
            help="The path to the new file. You may exclude the extension."
        )

        sub.add_argument(
            "-A=",
            "--archetype=",
            default="chapter",
            dest="archetype",
            help="Specify an archetype to use when creating the file."
        )

        sub.add_argument(
            "-V=",
            "--var=",
            action="append",
            dest="variables",
            help="Add a variable to the global context in the form of name:value."
        )

        self._add_common_options(sub)

    def preflight(self):
        """Initialize the preflight sub-command."""
        sub = self.subparsers.add_parser(
            "preflight",
            help="Check the project for potential problems prior to build."
        )

        self._add_common_options(sub)

    def skeleton(self):
        """Initialize the stats sub-command."""
        sub = self.subparsers.add_parser(
            "skeleton",
            aliases=["skel"],
            help="Build a project's structure."
        )

        sub.add_argument(
            "bones",
            help="Specify the skeleton to use for building the structure. Uses ? for the available options."
        )

        # sub.add_argument(
        #     "-s=",
        #     "--skeleton=",
        #     dest="skeleton",
        #     help="Use this skeleton for the project structure."
        # )

        self._add_common_options(sub)

    def stats(self):
        """Initialize the stats sub-command."""
        sub = self.subparsers.add_parser(
            "stats",
            help="Generate project statistics report."
        )

        self._add_common_options(sub)

    def todos(self):
        """Initialize the todos sub-command."""
        sub = self.subparsers.add_parser(
            "todos",
            help="List todos found in content."
        )

        self._add_common_options(sub)

    # noinspection PyMethodMayBeStatic
    def _add_common_options(self, sub):
        """Add the common switches to a given sub-command instance.

        :param subcommand: The sub-command instance.

        """

        sub.add_argument(
            "-D",
            "--debug",
            action="store_true",
            dest="debug_enabled",
            help="Enable debug mode. Produces extra output."
        )

        sub.add_argument(
            "-p",
            action="store_true",
            dest="preview_enabled",
            help="Preview the commands to be executed."
        )

        sub.add_argument(
            "-r=",
            "--root=",
            default=DEFAULT_PROJECT_ROOT,
            dest="project_root",
            help="The root directory of the project. Defaults to the current working directory."
        )

"""
A number of environment variables may be configured that automate or control QuickTome behavior.

``QUICKTOME_AUTHOR_NAME``
-------------------------

Used when creating a new project with the ``init`` command.

Default: ``None``

``QUICKTOME_AUTHOR_TITLE``
--------------------------

Used when creating a new project with the ``init`` command.

Default: ``None``

``QUICKTOME_COPYRIGHT_NAME``
----------------------------

Used when creating a new project with the ``init`` command.

Default: ``QUICKTOME_AUTHOR_NAME``

``QUICKTOME_LOGGER_NAME``
-------------------------

Primarily for programmatic use of the QuickTome library, this allows developers to change the logger used by QuickTome
when log statements are issued.

Default: ``quicktome``

``QUICKTOME_PROJECT_HOME``
--------------------------

Used by the ``ls`` command to locate QuickTome projects.

Default: ``None``

.. tip::
    If you are a Virtual Env User, the default will be that of ``$PROJECT_HOME``.

``QUICKTOME_PATH``
------------------

The path to the QuickTome install.

"""
import os

AUTHOR_NAME = os.environ.get("QUICKTOME_AUTHOR_NAME", None)
AUTHOR_TITLE = os.environ.get("QUICKTOME_AUTHOR_TITLE", None)
COPYRIGHT_NAME = os.environ.get("QUICKTOME_COPYRIGHT_NAME", AUTHOR_NAME)
LOGGER_NAME = os.environ.get("QUICKTOME_LOGGER_NAME", "quicktome")

DEFAULT_PROJECT_HOME = os.environ.get("PROJECT_HOME", None)
PROJECT_HOME = os.environ.get("QUICKTOME_PROJECT_HOME", DEFAULT_PROJECT_HOME)

QUICKTOME_PATH = os.path.abspath(os.path.dirname(__file__))

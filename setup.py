# See https://packaging.python.org/en/latest/distributing.html
# and https://docs.python.org/2/distutils/setupscript.html
# and https://pypi.python.org/pypi?%3Aaction=list_classifiers
from setuptools import setup, find_packages


def read_file(path):
    with open(path, "r") as f:
        contents = f.read()
        f.close()
    return contents


setup(
    name="python-quicktome",
    version=read_file("VERSION.txt"),
    description=read_file("DESCRIPTION.txt"),
    long_description=read_file("README.markdown"),
    author='Shawn Davis',
    author_email='shawn@jumpstarthq.com',
    url='https://github.com/develmaycare/python-quicktome/',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "awesome-slugify",
        "python-myninjas",
        "jinja2",
        "opml",
        "tabulate",
    ],
    dependency_links=[
        "https://bitbucket.com/myninjas/python-myninjas/master.tar.gz#python-myninjas",
    ],
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    zip_safe=False,
    tests_require=[
        "coverage",
        "pytest",
    ],
    test_suite='runtests.runtests',
    entry_points={
      'console_scripts': [
          'quicktome = quicktome.cli:quicktome_command',
      ],
    },
)
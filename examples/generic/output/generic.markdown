# Heading 1

## Images

Images are added normally:

![Example JPG](_static/example.jpg)

![Example PNG](_static/example.png)

## Includes

It is possible to include files relative to the current file's path:

    .. include:: path/to/included_file.markdown

Example:

### Include Me

This content was included!

## Tables

You may generate tables from a CSV file using the a short code:

    .. table:: name.csv
    
The ``name.csv`` file is assumed to be located in ``source/data/``. The first row of the CSV file will become the 
headers of the table. All remaining rows are used as table data.

Example:

column 1|column 2|column 3
--------|--------|--------
A|B|C
D|E|F
G|H|I
K|L|M

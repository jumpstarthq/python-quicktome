\newpage


























\begin{center}

{{ project.title }}

Edition {{ project.edition }}

Version {{ version }}

Published {{ current_month }}, {{ current_year }}

\end{center}


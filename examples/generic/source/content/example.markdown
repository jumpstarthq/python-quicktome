# Heading 1

## Images

Images are added normally:

![Example JPG](_images/example.jpg)

![Example PNG](_images/example.png)

## Includes

It is possible to include files relative to the current file's path:

    .. include:: path/to/included_file.markdown

Example:

.. include:: include-me.markdown

## Tables

You may generate tables from a CSV file using the a short code:

    .. table:: name.csv
    
The ``name.csv`` file is assumed to be located in ``source/data/``. The first row of the CSV file will become the 
headers of the table. All remaining rows are used as table data.

Example:

.. table:: example.csv
